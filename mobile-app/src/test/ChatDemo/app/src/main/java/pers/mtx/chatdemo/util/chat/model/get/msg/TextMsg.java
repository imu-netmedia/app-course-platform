package pers.mtx.chatdemo.util.chat.model.get.msg;




public class TextMsg {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TextMsg() {
    }
}
