//package pers.mtx.chatdemo.util.chat;
//
//import android.content.Intent;
//import android.util.Log;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import org.java_websocket.client.WebSocketClient;
//import org.java_websocket.handshake.ServerHandshake;
//
//import java.io.IOException;
//import java.net.URI;
//import java.net.URISyntaxException;
//import java.nio.charset.StandardCharsets;
//
//import pers.mtx.chatdemo.util.chat.model.get.ReceiveMsgVo;
//import pers.mtx.chatdemo.util.chat.model.get.ReturnMsgVo;
//import pers.mtx.chatdemo.util.chat.model.get.msg.TextMsg;
//
//public class WebSocketUtils  {
//    private String TAG = "web_socket";
//    private static WebSocketUtils instance = null;
//    private static ObjectMapper mapper = new ObjectMapper();
//    WebSocketClient client;
//    //转成的要操作的数据
//    URI uri;
//    //后台连接的url
//    String address = "ws://10.102.190.113:8889/ws";
//
//    //懒加载单例模式，synchronized确保线程安全。
//    // 适合没有多线程频繁调用的情况，多线程频繁调用的时候，可以用双重检查锁定DCL、静态内部类、枚举 等方法，文章后面会详细介绍。
//    public static synchronized WebSocketUtils getInstance() {
//        if (instance == null) {
//            instance = new WebSocketUtils();
//        }
//        return instance;
//    }
//
//    public void initSocket() {
//        if (null != client && client.isOpen()) {
//            return;
//        }
//        startSocket();
//    }
//
//    protected WebSocketUtils() {
//        initSocket();
//    }
//
//    public void startSocket() {
//        try {
//            uri = new URI(address);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//        if (null == client || !client.isOpen()) {
//            Log.d(TAG, "尝试建立: ");
//            client = new WebSocketClient(uri) {
//                @Override
//                public void onOpen(ServerHandshake serverHandshake) {
//                    Log.d(TAG, "建立suc ");
//                }
//
//                @Override
//                public void onMessage(String s) {
//                    //后台返回的消息
//                    Log.d(TAG, "返回: " + s);
//                    try {
//                        ReturnMsgVo msgVo = mapper.readValue(s.getBytes(StandardCharsets.UTF_8), ReturnMsgVo.class);
//                        if (msgVo.getStatus().equals(ReturnMsgVo.RECEIVE_MSG)) {
//                            ReceiveMsgVo receiveMsgVo = mapper.convertValue(msgVo.getMessage(), ReceiveMsgVo.class);
//                            if (receiveMsgVo.getMsgType().equals(ReceiveMsgVo.TEXT)) {
//                                TextMsg textMsg = mapper.convertValue(receiveMsgVo.getMessage(), TextMsg.class);
//                                Intent setAction = new Intent().setAction("pers.mtx.chatdemo.GET_TEXT_MSG");
//                                setAction.putExtra("data", textMsg.getText())
//                                        .putExtra("senderId", receiveMsgVo.getSenderId())
//                                        .putExtra("msgType", receiveMsgVo.getMsgType());
//
//                            }
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void onClose(int i, String s, boolean b) {
//
//                }
//
//                @Override
//                public void onError(Exception e) {
//                    e.printStackTrace();
//                    Log.w(TAG, e.getMessage());
//                }
//            };
//            Log.d(TAG, "ls: ");
//            client.connect();
//        }
//    }
//    //向服务器发送消息
//    public void sendMessage(Object s) {
//        if (client == null || client.isClosed() || !client.isOpen()) {
//            Log.d(TAG, "未建立实时通信");
//            return;
//        }
//        try {
//            Log.i(TAG, "发送: " + mapper.writeValueAsString(s));
//            client.send(mapper.writeValueAsString(s));
//        } catch (Exception e) {
//            e.printStackTrace();
//            startSocket();
//        }
//    }
//    //关闭socket
//    public void closeSocket() {
//        if (client != null) {
//            client.close();
//            client = null;
//        }
//    }
//
//
//
//}
