package pers.mtx.chatdemo.util.chat.model.get;



/**
 * @package: com.imu.chatmodule.LongConnection.model
 * @className: ReceiveMsgVo
 * @description: 返回的信息类
 * @author MaTaoxun
 * @date 2021/9/28 22:59
*/

public class ReceiveMsgVo {
    public final static Integer FILE = 5;
    public final static Integer VIDEO = 4;
    public final static Integer SOUND = 3;
    public final static Integer IMAGE = 2;
    public final static Integer TEXT = 1;

    private Integer msgType;
    private Object message;

    public ReceiveMsgVo() {
    }

    public Integer getMsgType() {
        return msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }
}
