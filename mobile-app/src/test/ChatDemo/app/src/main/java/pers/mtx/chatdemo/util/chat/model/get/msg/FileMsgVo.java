package pers.mtx.chatdemo.util.chat.model.get.msg;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;


import java.time.LocalDateTime;

/**
 * @package: com.imu.chatmodule.LongConnection.model.msg
 * @className: FileMsgVo
 * @description: 返回filemsg的类
 * @author MaTaoxun
 * @date 2021/10/10 15:20
*/

public class FileMsgVo {
    /**
     * 创建时间
     */
    private String time;



    /**
     * 发送方id
     */

    private String senderId;

    /**
     * 接收方id
     */

    private String receiverId;


    /**
     * 撤回
     */

    private String rb;


    private String msgId;
    private String fileName;
    private String url;

    public FileMsgVo(String time, String senderId, String receiverId, String rb, String msgId, String fileName, String url) {
        this.time = time;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.rb = rb;
        this.msgId = msgId;
        this.fileName = fileName;
        this.url = url;
    }

    public FileMsgVo() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getRb() {
        return rb;
    }

    public void setRb(String rb) {
        this.rb = rb;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
