package pers.mtx.chatdemo.util.chat.model.put;

/**
 * @author MaTaoxun
 * @package: com.imu.chatmodule.LongConnection.model.LongConnectionChatDTO.java
 * @className: LongConnectionChatDTO
 * @description: 向其他人单聊发送信息的DTO
 * @date 2021/9/28 22:41
 */
public class LongConnectionChatDTO {
    public final static Integer FILE = 5;
    public final static Integer VIDEO = 4;
    public final static Integer SOUND = 3;
    public final static Integer IMAGE = 2;
    public final static Integer TEXT = 1;

    // 通信id，仅作为服务端通知是否投递成功使用
    private String chatId;
    private String receiverId;
    private Object message;
    private Integer msgType;

    public LongConnectionChatDTO(String chatId, String receiverId, Object message, Integer msgType) {
        this.chatId = chatId;
        this.receiverId = receiverId;
        this.message = message;
        this.msgType = msgType;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getMsgType() {
        return msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }
}
