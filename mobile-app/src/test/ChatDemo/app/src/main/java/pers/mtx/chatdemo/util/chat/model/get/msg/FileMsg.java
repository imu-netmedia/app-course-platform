package pers.mtx.chatdemo.util.chat.model.get.msg;

import java.io.InputStream;

public class FileMsg {
    private String fileName;
    private byte[] stream;


    public FileMsg(String fileName, byte[] stream) {
        this.fileName = fileName;
        this.stream = stream;
    }



    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getStream() {
        return stream;
    }

    public void setStream(byte[] stream) {
        this.stream = stream;
    }
}
