package pers.mtx.chatdemo.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import pers.mtx.chatdemo.R;
import pers.mtx.chatdemo.service.WebSocketService;
import pers.mtx.chatdemo.util.chat.model.get.msg.FileMsg;
import pers.mtx.chatdemo.util.chat.model.get.msg.TextMsg;
import pers.mtx.chatdemo.util.chat.model.put.LongConnectionChatDTO;

public class SendActivity extends Activity {
    private WebSocketService webSocketService;
    private LinearLayout msgList;
    private TextView idText;
    private TextView msgText;
    private Button sendBtn;
    private Button fileBtn;
    private MsgBroadcastReceiver msgReceiver;

    public static final String ACTION_GET_TEXT_MSG = "mtx.action.GET_TEXT_MSG";


    private class MsgBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //这里是要在Activity活动里执行的代码
            Log.d("SendActivity", intent.getStringExtra("data")+intent.getStringExtra("senderId")+intent.getIntExtra("msgType",0));
            TextView textItem = new TextView(SendActivity.this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,200);
            textItem.setLayoutParams(layoutParams);
            textItem.setText("来自:"+intent.getStringExtra("senderId")+"的消息:"+intent.getStringExtra("data"));
            msgList.addView(textItem);
        }
    }
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder iBinder) {
            WebSocketService.WebSocketBinder binder = (WebSocketService.WebSocketBinder) iBinder;
            webSocketService = binder.getService();
            Log.d("ChatActivity", "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("ChatActivity", "onServiceDisconnected");
        }
    };





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_activity);
        Intent intent = new Intent(SendActivity.this, WebSocketService.class);

        bindService(intent,mServiceConnection,BIND_AUTO_CREATE);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_GET_TEXT_MSG);
        msgReceiver = new MsgBroadcastReceiver();
        registerReceiver(msgReceiver,intentFilter);

        msgText = findViewById(R.id.sendMsg);
        idText = findViewById(R.id.sendId);
        sendBtn = findViewById(R.id.sendBtn);
        msgList = findViewById(R.id.msgShow);
        fileBtn = findViewById(R.id.fileBtn);


        sendBtn.setOnClickListener(v -> {
            TextMsg textMsg = new TextMsg();
            textMsg.setText(msgText.getText().toString());
            LongConnectionChatDTO chatDTO = new LongConnectionChatDTO("testTxt",idText.getText().toString(),textMsg,LongConnectionChatDTO.TEXT);
            webSocketService.sendMessage(chatDTO);
        });

        fileBtn.setOnClickListener(v->{
            try {
                FileMsg fileMsg = new FileMsg("demo.png", webSocketService.getFromAssets("demo.png"));
                LongConnectionChatDTO chatDTO = new LongConnectionChatDTO("testFIle",idText.getText().toString(), fileMsg,
                        LongConnectionChatDTO.FILE);
                webSocketService.sendMessage(chatDTO);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(msgReceiver);
        unbindService(mServiceConnection);
        super.onDestroy();
    }
}
