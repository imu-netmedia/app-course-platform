package pers.mtx.chatdemo.util.chat.model.get.msg;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;


import java.time.LocalDateTime;

public class TextMsgVo {

    private String time;


    /**
     * 发送方id
     */

    private String senderId;

    /**
     * 接收方id
     */

    private String receiverId;

    /**
     * 消息
     */

    private String message;


    /**
     * 撤回
     */

    private String rb;


    private String msgId;

    public TextMsgVo(String time, String senderId, String receiverId, String message, String rb, String msgId) {
        this.time = time;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.message = message;
        this.rb = rb;
        this.msgId = msgId;
    }

    public TextMsgVo() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRb() {
        return rb;
    }

    public void setRb(String rb) {
        this.rb = rb;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }
}
