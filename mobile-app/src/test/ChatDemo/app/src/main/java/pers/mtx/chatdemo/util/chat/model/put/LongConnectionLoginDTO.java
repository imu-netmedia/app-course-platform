package pers.mtx.chatdemo.util.chat.model.put;


/**
 * @package: com.imu.chatmodule.LongConnection.model
 * @className: LongConnectionLoginDTO
 * @description: 登录的DTO
 * @author MaTaoxun
 * @date 2021/9/30 20:20
*/

public class LongConnectionLoginDTO {
    private String senderId;

    public LongConnectionLoginDTO(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
