package pers.mtx.chatdemo.util.chat.model.get;

/**
 * @package: com.imu.chatmodule.LongConnection.model
 * @className: SendMsgVo
 * @description: 给别人发信息返回的信息类
 * @author MaTaoxun
 * @date 2021/10/10 17:36
*/

public class SendMsgVo {
    private String chatId;
    private Object content;

    public SendMsgVo(String chatId, Object content) {
        this.chatId = chatId;
        this.content = content;
    }

    public SendMsgVo() {
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
