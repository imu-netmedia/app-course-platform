package pers.mtx.chatdemo.util.chat.model.get;



/**
 * @package: com.imu.chatmodule.LongConnection.model
 * @className: ReturnMsgVo
 * @description: 长连接服务返沪信息类
 * @author MaTaoxun
 * @date 2021/9/28 22:58
*/

public class ReturnMsgVo {


    //成功登录
    public static final Integer LOG_SUCCESS = 1;
    //数据包错误
    public static final Integer WRONG_PACKAGE = 2;
    //未登录
    public static final Integer WONT_LOG = 3;
    //成功发送
    public static final Integer SEND_SUCCESS = 4;
    //发送失败
    public static final Integer SEND_FAIL = 5;
    //踢出
    public static final Integer KICK_OFF = 6;
    //返回信息
    public static final Integer RECEIVE_MSG = 7;
    private Integer status;

    private Object message;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public ReturnMsgVo(){

    }
}
