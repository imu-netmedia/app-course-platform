package com.ruoyi.log.service.impl;

import com.ruoyi.log.domain.SysUserVisitHistory;
import com.ruoyi.log.domain.SysVisitLog;
import com.ruoyi.log.domain.SysVisitTimeLog;
import com.ruoyi.log.mapper.VisitHistoryMapper;
import com.ruoyi.log.service.IVisitHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: ruoyi
 * @description:
 * @author: Abner
 * @create: 2021-10-19 16:45
 */
@Service
public class VisitHistoryServiceImpl implements IVisitHistoryService {

    @Autowired
    VisitHistoryMapper visitHistoryMapper;


    @Override
    public List<SysUserVisitHistory> selectVisitHistory() {

        return visitHistoryMapper.selectVisitHistory();
    }

    @Override
    public List<SysVisitTimeLog> selectVisitTimeLog() {
        return visitHistoryMapper.selectVisitTimeLog();
    }

    @Override
    public List<SysVisitLog> selectVisitLogByTokenkey(String tokenkey) {
        return visitHistoryMapper.selectVisitLogByTokenkey(tokenkey);
    }


}
