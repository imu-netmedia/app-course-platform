package com.ruoyi.log.service.impl;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.data.entity.DataApp;
import com.ruoyi.data.service.IDataAppService;
import com.ruoyi.eduinfo.service.IAppUserService;
import com.ruoyi.log.domain.SysVisitLog;
import com.ruoyi.log.mapper.SysVisitLogMapper;
import com.ruoyi.log.service.ISysVisitLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: ruoyi
 * @description:访问日志 服务层处理
 * @author: Abner
 * @create: 2021-10-10 21:28
 */
@Service
public class SysVisitLogServiceImpl implements ISysVisitLogService {
    @Autowired
    private SysVisitLogMapper visitLogMapper;
    @Autowired
    private IDataAppService dataAppService;
    @Autowired
    private IAppUserService userService;

    @Override
    public void insertVisitlog(SysVisitLog sysVisitLog) {
        DataApp dataApp = dataAppService.selectById(sysVisitLog.getAppId());
        if (dataApp != null) {
            sysVisitLog.setVisitName(dataApp.getAppName());
            if (visitLogMapper.selectVisitlog(sysVisitLog)==0) {
                visitLogMapper.insertVisitlog(sysVisitLog);
            }

        }

    }

    @Override
    public void updateInvokeCount(Long userid, String url, String tokenKey, Long invokeCount) {
        visitLogMapper.updateInvokeCount(userid,url,tokenKey,invokeCount);

    }

    @Override
    public List<SysVisitLog> selectVisitLogInfoByAppId(Long appId) {
        return visitLogMapper.selectVisitLogInfoByAppId(appId);
    }


}
