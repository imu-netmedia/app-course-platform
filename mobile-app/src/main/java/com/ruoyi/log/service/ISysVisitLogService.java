package com.ruoyi.log.service;

import com.ruoyi.log.domain.SysVisitLog;

import java.util.List;

public interface ISysVisitLogService {
    /**
     * 新增操作日志
     *
     * @param sysVisitLog 操作日志对象
     */
    public void insertVisitlog(SysVisitLog sysVisitLog);


    public void updateInvokeCount(Long userid, String url, String tokenKey, Long invokeCount);

    List<SysVisitLog> selectVisitLogInfoByAppId(Long appId);

}
