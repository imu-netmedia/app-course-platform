package com.ruoyi.log.service;

import com.ruoyi.log.domain.SysVisitTimeLog;

public interface ISysVisitTimeService {

    void insertVisitTimeLog(SysVisitTimeLog sysVisitTimeLog);

    SysVisitTimeLog getVisitTimeLogbyToken(String key);

    void updateVisitTimeLog(SysVisitTimeLog sysVisitTimeLog);
}
