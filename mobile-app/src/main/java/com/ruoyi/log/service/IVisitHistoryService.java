package com.ruoyi.log.service;

import com.ruoyi.log.domain.SysUserVisitHistory;
import com.ruoyi.log.domain.SysVisitLog;
import com.ruoyi.log.domain.SysVisitTimeLog;

import java.util.List;

/**
 * @program: ruoyi
 * @description:访问日志信息服务
 * @author: Abner
 * @create: 2021-10-19 16:26
 */
public interface IVisitHistoryService {

    List<SysUserVisitHistory> selectVisitHistory();

    List<SysVisitTimeLog> selectVisitTimeLog();

    List<SysVisitLog> selectVisitLogByTokenkey(String tokenkey);
}
