package com.ruoyi.log.service.impl;

import com.ruoyi.log.domain.SysVisitTimeLog;
import com.ruoyi.log.mapper.SysVisitTimeMapper;
import com.ruoyi.log.service.ISysVisitTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: ruoyi
 * @description:
 * @author: Abner
 * @create: 2021-10-14 13:50
 */
@Service
public class SysVisitTimeServiceImpl implements ISysVisitTimeService {

    @Autowired
    private SysVisitTimeMapper visitTimeMapper;

    @Override
    public void insertVisitTimeLog(SysVisitTimeLog sysVisitTimeLog) {
        int count = visitTimeMapper.selectVisitTimeLog(sysVisitTimeLog);
        if (count == 0) {
            visitTimeMapper.insertVisitTimeLog(sysVisitTimeLog);
        }
    }

    @Override
    public SysVisitTimeLog getVisitTimeLogbyToken(String key) {
        return visitTimeMapper.selectVisitTimeLogbyToken(key);
    }

    @Override
    public void updateVisitTimeLog(SysVisitTimeLog sysVisitTimeLog) {
        visitTimeMapper.updateVisitTimeLog(sysVisitTimeLog);
    }

}
