package com.ruoyi.log.domain;

import java.util.List;

/**
 * @program: ruoyi
 * @description:用户范文API信息
 * @author: Abner
 * @create: 2021-10-19 16:33
 */
public class SysUserVisitHistory {
    /*访问的用户*/
    private SysVisitTimeLog sysVisitTimeLog;
    /*访问的api*/
    private List<SysVisitLog> sysVisitLog;

    public SysVisitTimeLog getSysVisitTimeLog() {
        return sysVisitTimeLog;
    }

    public void setSysVisitTimeLog(SysVisitTimeLog sysVisitTimeLog) {
        this.sysVisitTimeLog = sysVisitTimeLog;
    }

    public List<SysVisitLog> getSysVisitLog() {
        return sysVisitLog;
    }

    public void setSysVisitLog(List<SysVisitLog> sysVisitLog) {
        this.sysVisitLog = sysVisitLog;
    }
}
