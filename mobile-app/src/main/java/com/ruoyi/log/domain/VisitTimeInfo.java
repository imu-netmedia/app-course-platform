package com.ruoyi.log.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 *  日志对象 visit_time_info
 * 
 * @author ruoyi
 * @date 2021-12-09
 */
public class VisitTimeInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;



    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long appId;

    /** 学号*/
    @Excel(name = "学号")
    private String visitName;

    /** 访问时长 */
    @Excel(name = "访问时长")
    private String timeLong;

    /** 调用次数 */
    @Excel(name = "调用次数")
    private Long count;

    /**  信息 */
    private List<SysVisitLog> visitLogInfoList;


    public void setAppId(Long appId) 
    {
        this.appId = appId;
    }

    public Long getAppId() 
    {
        return appId;
    }

    public String getVisitName() {
        return visitName;
    }

    public void setVisitName(String visitName) {
        this.visitName = visitName;
    }

    public String getTimeLong() {
        return timeLong;
    }

    public void setTimeLong(String timeLong) {
        this.timeLong = timeLong;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<SysVisitLog> getVisitLogInfoList()
    {
        return visitLogInfoList;
    }

    public void setVisitLogInfoList(List<SysVisitLog> visitLogInfoList)
    {
        this.visitLogInfoList = visitLogInfoList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("appId", getAppId())
                .append("visitName", getVisitName())
                .append("timeLong", getTimeLong())
                .append("count", getCount())
                .append("visitLogInfoList", getVisitLogInfoList())
                .toString();
    }
}
