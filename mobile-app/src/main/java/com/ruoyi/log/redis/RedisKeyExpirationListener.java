package com.ruoyi.log.redis;


import com.ruoyi.framework.manager.AsyncManager;
import com.ruoyi.log.manager.factory.AsyncLogFactory;
import com.ruoyi.log.service.ISysVisitTimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;


/**
 * @program: ruoyi
 * @description:
 * @author: Abner
 * @create: 2021-10-12 21:44
 */
@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {
    @Autowired
    ISysVisitTimeService sysVisitTimeService;
    private static final Logger log = LoggerFactory.getLogger(RedisKeyExpirationListener.class);

    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }




    /**
     * redis过期 key回调
     *
     * @param message
     * @param pattern
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        // 获取失效的key
        String expiredKey = message.toString();
        //记录信息
        AsyncManager.me().execute(AsyncLogFactory.recordUserLogout(expiredKey));

    }

}
