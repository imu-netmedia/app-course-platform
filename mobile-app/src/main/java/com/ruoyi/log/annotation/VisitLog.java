package com.ruoyi.log.annotation;



import java.lang.annotation.*;

/**
 * @program: ruoyi
 * @description:访问日志
 * @author: Abner
 * @create: 2021-10-19 08:03
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface VisitLog {

}
