package com.ruoyi.log.mapper;

import com.ruoyi.log.domain.SysVisitLog;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: ruoyi
 * @description:
 * @author: Abner
 * @create: 2021-10-10 21:35
 */
@Repository
public interface SysVisitLogMapper {
    /**
     * 新增操作日志
     *
     * @param sysVisitLog 操作日志对象
     */
    public void insertVisitlog(SysVisitLog sysVisitLog);



    public  void updateInvokeCount(Long userid, String url, String tokenKey, Long invokeCount);

    public int selectVisitlog(SysVisitLog sysVisitLog);

    List<SysVisitLog> selectVisitLogInfoByAppId(Long appId);
}
