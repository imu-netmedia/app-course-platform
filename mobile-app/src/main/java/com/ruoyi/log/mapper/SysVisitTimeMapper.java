package com.ruoyi.log.mapper;

import com.ruoyi.log.domain.SysVisitTimeLog;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SysVisitTimeMapper {

    void insertVisitTimeLog(SysVisitTimeLog sysVisitTimeLog);

    int selectVisitTimeLog(SysVisitTimeLog sysVisitTimeLog);

    SysVisitTimeLog selectVisitTimeLogbyToken(@Param("tokenKey") String key);

    void updateVisitTimeLog(SysVisitTimeLog sysVisitTimeLog);
}
