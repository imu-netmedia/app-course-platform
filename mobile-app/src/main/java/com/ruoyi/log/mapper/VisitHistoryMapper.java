package com.ruoyi.log.mapper;

import com.ruoyi.log.domain.SysUserVisitHistory;
import com.ruoyi.log.domain.SysVisitLog;
import com.ruoyi.log.domain.SysVisitTimeLog;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: ruoyi
 * @description： 访问历史
 * @author: Abner
 * @create: 2021-10-19 16:48
 */
@Repository
public interface VisitHistoryMapper {
    List<SysUserVisitHistory> selectVisitHistory();

    List<SysVisitTimeLog> selectVisitTimeLog();

    List<SysVisitLog> selectVisitLogByTokenkey(String tokenkey);
}
