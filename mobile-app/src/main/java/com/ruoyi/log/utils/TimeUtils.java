package com.ruoyi.log.utils;

public class TimeUtils {
    public static String convertTime(Long time) {
        long hour = time/(60*60*1000);
        long minute = (time-hour*60*60*1000)/(60*1000);
        long second = (time-hour*60*60*1000-minute*60*1000)/1000;
        if(second >= 60 )
        {
            second = second % 60;
            minute+=second/60;
        }
        if(minute >= 60)
        {
            minute = minute %60;
            hour += minute/60;
        }
        String sh ="";
        String sm ="";
        String ss ="";
        if(hour <10)
        {
            sh ="" + String.valueOf(hour);
        }else
        {
            sh = String.valueOf(hour);
        }
        if(minute <10)
        {
            sm = "" + String.valueOf(minute);
        }else
        {
            sm = String.valueOf(minute);
        }
        if(second <10)
        {
            ss =""+ String.valueOf(second);
        }else
        {
            ss = String.valueOf(second);
        }
        return hour + "h" + minute + "m" + second + "s";
    }
}
