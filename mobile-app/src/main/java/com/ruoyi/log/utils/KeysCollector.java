package com.ruoyi.log.utils;

import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.spring.SpringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class KeysCollector {

    private static Map<String, Map<String, Long>> keysMap = new HashMap<>();

    public static void addKey(String appRedisKey, String visitKey, final long millisecond, Long count) {
        RedisCache redisService = SpringUtils.getBean(RedisCache.class);
        redisService.setCacheMapValue(appRedisKey, visitKey, count);
        //设置redis的过期时间
        redisService.expire(appRedisKey, millisecond, TimeUnit.MINUTES);
        Map<String,Long> appMap = redisService.getCacheMap(appRedisKey);
        keysMap.put(appRedisKey, appMap);
    }

    public static void removeKey(String appRedisKey) {
        keysMap.remove(appRedisKey);
    }

    public static Map<String, Map<String, Long>> getKeysMap() {
        return keysMap;
    }

}
