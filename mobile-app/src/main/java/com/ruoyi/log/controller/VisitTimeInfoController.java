package com.ruoyi.log.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.log.domain.SysVisitLog;
import com.ruoyi.log.domain.SysVisitTimeLog;
import com.ruoyi.log.service.ISysVisitLogService;
import com.ruoyi.log.service.IVisitHistoryService;
import com.ruoyi.log.utils.TimeUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.log.domain.VisitTimeInfo;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 *  日志Controller
 * 
 * @author ruoyi
 * @date 2021-12-09
 */
@RestController
@RequestMapping("/log/applog")
public class VisitTimeInfoController extends BaseController
{


    @Autowired
    IVisitHistoryService visitHistoryService;
    @Autowired
    ISysVisitLogService visitLogService;

    /**
     * 查询 日志列表
     */
    @PreAuthorize("@ss.hasPermi('log:applog:list')")
    @GetMapping("/list")
    public TableDataInfo list(VisitTimeInfo visitTimeInfo)
    {
        List<SysVisitTimeLog> visitTimeLogs = visitHistoryService.selectVisitTimeLog();
        Map<Long, Long> temp = new HashMap<>();
        //统计访问时间
        for (SysVisitTimeLog visitTimeLog : visitTimeLogs) {
            if (visitTimeLog != null) {
                Long appId = visitTimeLog.getAppId();
                Long ltimelong = temp.get(appId);
                Long ntimeLong = visitTimeLog.getTimeLong();
                if (ntimeLong != null) {
                    if (ltimelong == null) {
                        temp.put(appId, ntimeLong);
                    } else {
                        temp.put(appId, ntimeLong + ltimelong);
                    }
                }
            }
        }
        //最后结果
        List<VisitTimeInfo> visitTimeInfos = new ArrayList<>();
        for (Long id : temp.keySet()) {
            VisitTimeInfo timeInfo = new VisitTimeInfo();
            timeInfo.setAppId(id);
            timeInfo.setTimeLong(TimeUtils.convertTime(temp.get(id)));
            visitTimeInfos.add(timeInfo);
        }
        for (VisitTimeInfo timeInfo : visitTimeInfos) {
            List<SysVisitLog> sysVisitLogs = visitLogService.selectVisitLogInfoByAppId(timeInfo.getAppId());
            if (sysVisitLogs.size()>0) {
                Long sum = 0l;
                for (SysVisitLog sysVisitLog : sysVisitLogs) {
                    Long invokeCount = sysVisitLog.getInvokeCount();
                    if (invokeCount != null) {
                        sum += invokeCount;
                    }
                }
                timeInfo.setCount(sum);
                timeInfo.setVisitName(sysVisitLogs.get(sysVisitLogs.size()-1).getVisitName());
            }
        }
        startPage();
        return getDataTable(visitTimeInfos);
    }

    /**
     * 查询 具体用户访问列表
     */
    @PreAuthorize("@ss.hasPermi('log:applog:query')")
    @GetMapping("/details")
    public TableDataInfo listDetails(String appId) {
        startPage();
        List<SysVisitLog> sysVisitLogs = visitLogService.selectVisitLogInfoByAppId(Long.valueOf(appId));
        return getDataTable(sysVisitLogs);
    }



}
