package com.ruoyi.eduinfo.controller.api;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.data.entity.DataApp;
import com.ruoyi.data.service.IDataAppService;
import com.ruoyi.eduinfo.model.Contacts;
import com.ruoyi.eduinfo.service.IAppUserService;
import com.ruoyi.log.annotation.VisitLog;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * App用户API接口
 * @author Zhangxiliang
 * @date 2021-11-05
 */
@RestController
@RequestMapping("/app/user/api")
public class AppUserApiController extends BaseController {

    @Autowired
    private IDataAppService dataAppService;
    @Autowired
    private IAppUserService userService;
    @Autowired
    private ISysUserService sysUserService;
    /**
     * 获取联系人列表
     */
    @VisitLog
    @GetMapping("/list")
    public AjaxResult list(Contacts contacts)
    {
        if(com.ruoyi.common.utils.StringUtils.isNotNull(contacts.getClassesId())) {
            List<Contacts> list = userService.selectContactsList(contacts);
            return AjaxResult.success(list);
        }
        else
            return AjaxResult.error(401,"请求参数错误，classesId不能为空");
    }

    /**
     * 根据用户编号获取详细信息
     */
    @VisitLog
    @GetMapping(value = {"/getById" })
    public AjaxResult getInfo(Long userId)
    {
        AjaxResult ajax = AjaxResult.success();
        if (com.ruoyi.common.utils.StringUtils.isNotNull(userId))
            ajax.put(AjaxResult.DATA_TAG, userService.selectContactsById(userId));
        return ajax;
    }

    /**
     * 根据用户名获取详细信息
     */
    @VisitLog
    @GetMapping(value = { "/getByUserName" })
    public AjaxResult getInfo(String userName)
    {
        AjaxResult ajax = AjaxResult.success();
        if (com.ruoyi.common.utils.StringUtils.isNotEmpty(userName))
            ajax.put(AjaxResult.DATA_TAG, userService.selectContactsByUserName(userName));
        return ajax;
    }

    /**
     * app用户注册
     * @param user
     * @return
     */
    @VisitLog
    @PostMapping("/register")
    public AjaxResult register(@RequestBody SysUser user,Long appId)
    {
        DataApp dataApp = dataAppService.selectById(appId);
        user.setCreateBy(dataApp.getAppName());
        if (user.getClassesId() == null)
        {
            SysUser sysUser = sysUserService.selectUserById(dataApp.getUserId());
            user.setClassesId(sysUser.getClassesId());
        }
        String msg = userService.registerUser(user);
        return StringUtils.isEmpty(msg) ? success() : error(msg);
    }

    /**
     * app用户登录
     * @param user
     * @return
     */
    @VisitLog
    @PostMapping("/login")
    public AjaxResult login(@RequestBody SysUser user)
    {
        AjaxResult ajax = AjaxResult.success();
        if(com.ruoyi.common.utils.StringUtils.isNotEmpty(user.getUserName())) {
            SysUser u = userService.login(user);
            if (u != null) {
                ajax.put("userId", u.getUserId());
                ajax.put("userName", u.getUserName());
                ajax.put("nickName",u.getNickName());
                ajax.put("classesId", u.getClassesId());
                return ajax;
            }
        }
        return AjaxResult.error("登录失败");

    }
    /**
     * 修改用户
     */
    @VisitLog
    @PostMapping("/edit")
    public AjaxResult edit(@Validated @RequestBody SysUser user)
    {
        userService.checkUserAllowed(user);
        if (com.ruoyi.common.utils.StringUtils.isNotEmpty(user.getPhonenumber())
                && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user)))
        {
            return AjaxResult.error("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        else if (com.ruoyi.common.utils.StringUtils.isNotEmpty(user.getEmail())
                && UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user)))
        {
            return AjaxResult.error("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        user.setUpdateBy(user.getUpdateBy());
        return toAjax(userService.updateUser(user));
    }

    /**
     * 重置密码
     */
    @VisitLog
    @PostMapping("/resetPwd")
    public AjaxResult resetPwd(@RequestBody SysUser user)
    {
        userService.checkUserAllowed(user);
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        user.setUpdateBy(user.getUpdateBy());
        return toAjax(userService.resetPwd(user));
    }


}
