package com.ruoyi.eduinfo.controller.api;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysClasses;
import com.ruoyi.eduinfo.service.IClassesService;
import com.ruoyi.log.annotation.VisitLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * APP请求班级相关信息的API接口
 * @author zhangxiliang
 */

@RestController
@RequestMapping("/classes/api")
public class ClassesController {

      @Autowired
      private IClassesService classesService;

     /**
      * 获取班级列表
      */
     @VisitLog
     @GetMapping("/list")
     public AjaxResult list(SysClasses classes )
     {
       List<SysClasses> classess = classesService.selectClassesList(classes);

      return AjaxResult.success(classess);
     }

    /**
     * 根据班级编号获取详细信息
     */
    @VisitLog
    @GetMapping(value = "/getClasses")
    public AjaxResult getInfo(Long classesId)
    {
        return AjaxResult.success(classesService.selectClassesById(classesId));
    }


}
