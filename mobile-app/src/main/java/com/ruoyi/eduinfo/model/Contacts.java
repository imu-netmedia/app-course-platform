package com.ruoyi.eduinfo.model;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;


/**
 * 返回联系人列表实体类 contacts
 * 
 * @author zhangxiliang
 */
@Data
public class Contacts extends BaseEntity
{

    /** 用户ID */
    private Long userId;

    /** 班级ID */
    private Long classesId;

    /** 学号 */
    private String userName;

    /** 姓名 */
    private String nickName;

    /** 用户邮箱 */
    private String email;

    /** 手机号码 */
    private String phonenumber;

    /** 用户性别 */
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 班级对象 */
    private Classes classes;

}
