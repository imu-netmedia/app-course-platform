package com.ruoyi.eduinfo.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 返回班级列表实体类
 * 
 * @author zhangxiliang
 */
@Data
public class Classes
{
    /** 班级ID */
    private Long classesId;

    /** 父班级ID */
    private Long parentId;

    /** 祖级列表 */
    private String ancestors;

    /** 班级名称 */
    private String classesName;

    /** 显示顺序 */
    private String orderNum;

    /** 负责人 */
    private String leader;

    /** 联系电话 */
    private String phone;

    /** 邮箱 */
    private String email;

    /** 父班级名称 */
    private String parentName;
    
    /** 子班级 */
    private List<Classes> children = new ArrayList<Classes>();

}
