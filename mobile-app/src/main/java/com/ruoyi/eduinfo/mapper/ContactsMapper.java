package com.ruoyi.eduinfo.mapper;


import com.ruoyi.eduinfo.model.Contacts;

import java.util.List;

/**
 * 联系人信息 数据层
 *
 * @author zhangxiliang
 */
public interface ContactsMapper {
    /**
     * 查询班级联系人数据
     *
     * @param contacts 联系人信息
     * @return 联系人信息集合
     */
    public List<Contacts> selectUserList(Contacts contacts);


    /**
     * 通过用户名查询联系人
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public Contacts selectUserByUserName(String userName);

    /**
     * 通过用户ID查询联系人
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public Contacts selectUserById(Long userId);

}
