package com.ruoyi.eduinfo.mapper;

import com.ruoyi.common.core.domain.entity.SysClasses;


import java.util.List;

/**
 * 班级信息 数据层
 * 
 * @author zhangxiliang
 */
public interface ClassesMapper
{
    /**
     * 查询班级管理数据
     * 
     * @param classes 班级信息
     * @return 班级信息集合
     */
    public List<SysClasses> selectClassesList(SysClasses classes);

    /**
     * 根据班级ID查询信息
     * 
     * @param classesId 班级ID
     * @return 班级信息
     */
    public SysClasses selectClassesById(Long classesId);

    /**
     * 根据ID查询所有子班级
     * 
     * @param classesId 班级ID
     * @return 班级列表
     */
    public List<SysClasses> selectChildrenClassesById(Long classesId);


    /**
     * 根据ID查询所有子班级（正常状态）
     * 
     * @param classesId 班级ID
     * @return 子班级数
     */
    public int selectNormalChildrenClassesById(Long classesId);

    /**
     * 是否存在子节点
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    public int hasChildByClassesId(Long classesId);

    /**
     * 查询班级是否存在用户
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    public int checkClassesExistUser(Long classesId);

}
