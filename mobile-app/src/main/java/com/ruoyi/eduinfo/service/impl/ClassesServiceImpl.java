package com.ruoyi.eduinfo.service.impl;

import com.ruoyi.common.core.domain.entity.SysClasses;

import com.ruoyi.eduinfo.mapper.ClassesMapper;
import com.ruoyi.eduinfo.service.IClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;


/**
 * 班级信息 服务实现
 * 
 * @author ruoyi
 */
@Service
public class ClassesServiceImpl implements IClassesService
{
    @Autowired
    private ClassesMapper classesMapper;

    /**
     * 查询班级管理数据
     * 
     * @param classes 班级信息
     * @return 班级信息集合
     */
    @Override
    public List<SysClasses> selectClassesList(SysClasses classes)
    {
        return classesMapper.selectClassesList(classes);
    }

    /**
     * 根据班级ID查询信息
     * 
     * @param classesId 班级ID
     * @return 班级信息
     */
    @Override
    public SysClasses selectClassesById(Long classesId)
    {
        return classesMapper.selectClassesById(classesId);
    }

    /**
     * 根据ID查询所有子班级（正常状态）
     * 
     * @param classesId 班级ID
     * @return 子班级数
     */
    @Override
    public int selectNormalChildrenClassesById(Long classesId)
    {
        return classesMapper.selectNormalChildrenClassesById(classesId);
    }

    /**
     * 是否存在子节点
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    @Override
    public boolean hasChildByClassesId(Long classesId)
    {
        int result = classesMapper.hasChildByClassesId(classesId);
        return result > 0 ? true : false;
    }

    /**
     * 查询班级是否存在用户
     * 
     * @param classesId 班级ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkClassesExistUser(Long classesId)
    {
        int result = classesMapper.checkClassesExistUser(classesId);
        return result > 0 ? true : false;
    }

}
