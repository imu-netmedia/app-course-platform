package com.ruoyi.eduinfo.service;

import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.core.domain.entity.SysClasses;

import java.util.List;

/**
 * 班级信息 服务层
 * 
 * @author zhangxiliang
 */
public interface IClassesService
{
    /**
     * 查询班级管理数据
     * 
     * @param classes 班级信息
     * @return 班级信息集合
     */
    public List<SysClasses> selectClassesList(SysClasses classes);

    /**
     * 根据班级ID查询信息
     * 
     * @param classesId 班级ID
     * @return 班级信息
     */
    public SysClasses selectClassesById(Long classesId);

    /**
     * 根据ID查询所有子班级（正常状态）
     * 
     * @param classesId 班级ID
     * @return 子班级数
     */
    public int selectNormalChildrenClassesById(Long classesId);

    /**
     * 是否存在班级子节点
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    public boolean hasChildByClassesId(Long classesId);

    /**
     * 查询班级是否存在用户
     * 
     * @param classesId 班级ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkClassesExistUser(Long classesId);

}
