package com.ruoyi.chat.service;

import com.ruoyi.chat.ab.entity.ChatRecord;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author MaTaoxun
 * @package: com.ruoyi.chat.service
 * @className: ChatServiceImpl
 * @description: 聊天服务类
 * @date 2021/9/28 22:45
 */
public interface IChatService {
    /**
     * @param receiverId 消息收取人的id
     * @param bytes      信息
     * @return 是否成功
     * @description: 给对应id的用户发消息
     * @author MaTaoxun
     * @date 2021/9/28 22:49
     */
    boolean sendMessageById(String receiverId,byte[] bytes);

    /**
     * @param id 用户id
     * @return
     * @description: 获取id未推送的消息
     * @author MaTaoxun
     * @date 2021/10/1 20:37
     */
    List<ChatRecord> getAllUnReadMsg(String id);

    /**
     * @param id 用户id
     * @return
     * @description: 获取与当前id有通信的所有人的三条信息
     * @author MaTaoxun
     * @date 2021/10/1 21:51
     */
    List<ChatRecord> getAllFriendsThreeHistoryChats(String id);

    /**
    * @description: 获取五条聊天记里
    * @param
    * @return
    * @author MaTaoxun
    * @date 2021/10/2 19:32
    */
    List<ChatRecord> getFiveHisChats(String msgId,String user1,String user2);

    /**
    * @description: 通过日期获取聊天记录
    * @param
    * @return
    * @author MaTaoxun
    * @date 2021/10/2 19:32
    */
    List<ChatRecord> getHisChatsBetweenDate(String Id,String Id2, LocalDateTime bg, LocalDateTime ed);
}
