package com.ruoyi.chat.service;

import com.ruoyi.chat.LongConnection.model.msg.FileMsg;

public interface IMinioService {
    String setFile(FileMsg fileMsg) throws Exception;
}
