package com.ruoyi.chat.config;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChatConfig {

    @Value("${chat.minio.url}")
    private String minioUrl;
    @Value("${chat.minio.user}")
    private String minioUser;
    @Value("${chat.minio.pwd}")
    private String minioPwd;

    @Bean
    public MinioClient minio() {
        return MinioClient.builder()
                .endpoint(minioUrl)
                .credentials(minioUser, minioPwd)
                .build();
    }
}
