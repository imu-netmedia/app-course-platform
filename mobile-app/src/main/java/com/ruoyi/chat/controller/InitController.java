package com.ruoyi.chat.controller;

import com.ruoyi.chat.model.vo.MessageVo;
import com.ruoyi.chat.service.IChatService;
import com.ruoyi.chat.util.MapperUtil;
import com.ruoyi.log.annotation.VisitLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @package: com.ruoyi.chat.controller
 * @className: initController
 * @description: 第一次登录需要的数据
 * @author MaTaoxun
 * @date 2021/10/1 20:32
 * 未处理，统一返回格式
*/
@RestController
@RequestMapping("/chat/init")
public class InitController {
    @Autowired
    IChatService chatService;

    /**
    * @description: 获取未被推送的信息
    * @param id
    * @return
    * @author MaTaoxun
    * @date 2021/10/2 19:27
    */
    @VisitLog
    @RequestMapping("/UnReadChats")
    public List<MessageVo> InitializeChats(@RequestBody String id){
        return MapperUtil.copyObjects(chatService.getAllUnReadMsg(id), MessageVo.class);
    }

    /**
     * @description: 获取初始化信息
     * @param id
     * @return
     * @author MaTaoxun
     * @date 2021/10/2 19:27
     */
    @VisitLog
    @PostMapping("/InitHistoryChats")
    public List<MessageVo> InitHistoryChats(@RequestBody String id){
        return MapperUtil.copyObjects(chatService.getAllFriendsThreeHistoryChats(id), MessageVo.class);
    }

}
