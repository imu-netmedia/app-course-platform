package com.ruoyi.chat.controller;

import com.ruoyi.chat.ab.entity.ChatRecord;
import com.ruoyi.chat.model.dto.DateRangeDTO;
import com.ruoyi.chat.model.dto.GetFiveMsgDTO;
import com.ruoyi.chat.model.vo.MessageVo;
import com.ruoyi.chat.service.IChatService;
import com.ruoyi.chat.util.MapperUtil;
import com.ruoyi.chat.service.IChatService;
import com.ruoyi.log.annotation.VisitLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author MaTaoxun
 * @package: com.ruoyi.chat.controller
 * @className: ChatHistoryController
 * @description: 历史记录控制器
 * @date 2021/10/2 10:23
 */
@RestController
@RequestMapping("/chat/history")
public class ChatHistoryController {
    @Autowired
    IChatService chatService;

    /**
    * @description: 获取该msg之上的五条msg
    * @param fiveMsgDTO
    * @return
    * @author MaTaoxun
    * @date 2021/10/2 19:23
    */
    @VisitLog
    @RequestMapping("/getFiveMsg")
    public List<MessageVo> getFiveMsg(@RequestBody GetFiveMsgDTO fiveMsgDTO) {
        //System.out.println(fiveMsgDTO);
        List<ChatRecord> fiveHisChats = chatService.getFiveHisChats(fiveMsgDTO.getMsgId(), fiveMsgDTO.getId1(), fiveMsgDTO.getId2());
        List<MessageVo> messageVos = MapperUtil.copyObjects(fiveHisChats, MessageVo.class);
        messageVos.sort(new MessageVo.MessageVoCompare());
        return messageVos;
    }

    /**
    * @description: 根据日期获取聊天记录
    * @param dateRangeDTO
    * @return
    * @author MaTaoxun
    * @date 2021/10/2 19:26
    */
    @VisitLog
    @RequestMapping("/getHistoryChatsByDate")
    public List<MessageVo> getHistoryChatsByDate(@RequestBody DateRangeDTO dateRangeDTO) {
        List<ChatRecord> chatrecordList = chatService.getHisChatsBetweenDate(dateRangeDTO.getId1(), dateRangeDTO.getId2(),
                dateRangeDTO.getBeginTime(), dateRangeDTO.getEndTime());
        List<MessageVo> messageVos = MapperUtil.copyObjects(chatrecordList, MessageVo.class);
        messageVos.sort(new MessageVo.MessageVoCompare());
        return messageVos;
    }

}
