package com.ruoyi.chat.ab.mapper;

import com.ruoyi.chat.ab.entity.ChatFileData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 文件与请求地址 Mapper 接口
 * </p>
 *
 * @author mataoxun
 * @since 2021-10-11
 */
@Mapper
public interface ChatFileDataMapper extends BaseMapper<ChatFileData> {

}
