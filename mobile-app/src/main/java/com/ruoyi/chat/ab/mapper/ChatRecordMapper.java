package com.ruoyi.chat.ab.mapper;

import com.ruoyi.chat.ab.entity.ChatRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 聊天记录  Mapper 接口
 * </p>
 *
 * @author mataoxun
 * @since 2021-10-11
 */
@Mapper
public interface ChatRecordMapper extends BaseMapper<ChatRecord> {

}
