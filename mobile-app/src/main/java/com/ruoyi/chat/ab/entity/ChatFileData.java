package com.ruoyi.chat.ab.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.annotations.Delete;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 文件与请求地址
 * </p>
 *
 * @author mataoxun
 * @since 2021-10-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("chat_file_data")
public class ChatFileData implements Serializable {

    private static final long serialVersionUID = 1L;

    public ChatFileData(String msgId, String url, String fileName) {
        this.msgId = msgId;
        this.url = url;
        this.fileName = fileName;
    }

    /**
     * 消息id
     */
    @NotNull
    @TableId(value = "msg_id", type = IdType.ASSIGN_UUID)
    private String msgId;

    /**
     * 文件请求地址
     */
    @NotNull
    @TableField("url")
    private String url;

    /**
     * 文件名称
     */
    @NotNull
    @TableField("file_name")
    private String fileName;

    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createdTime;

    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updatedTime;

    @TableField(value = "deleted",fill = FieldFill.INSERT)
    @TableLogic(value = "F",delval = "T")
    private String deleted;


}
