package com.ruoyi.chat.ab.service;

import com.ruoyi.chat.ab.entity.ChatRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 聊天记录  服务类
 * </p>
 *
 * @author mataoxun
 * @since 2021-10-11
 */
public interface IChatRecordService extends IService<ChatRecord> {

}
