package com.ruoyi.chat.ab.service;

import com.ruoyi.chat.ab.entity.ChatFileData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文件与请求地址 服务类
 * </p>
 *
 * @author mataoxun
 * @since 2021-10-11
 */
public interface IChatFileDataService extends IService<ChatFileData> {

}
