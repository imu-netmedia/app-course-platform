package com.ruoyi.chat.ab.service.impl;

import com.ruoyi.chat.ab.entity.ChatRecord;
import com.ruoyi.chat.ab.mapper.ChatRecordMapper;
import com.ruoyi.chat.ab.service.IChatRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 聊天记录  服务实现类
 * </p>
 *
 * @author mataoxun
 * @since 2021-10-11
 */
@Service
public class ChatRecordServiceImpl extends ServiceImpl<ChatRecordMapper, ChatRecord> implements IChatRecordService {

}
