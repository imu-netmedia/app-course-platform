package com.ruoyi.chat.ab.service.impl;

import com.ruoyi.chat.ab.entity.ChatFileData;
import com.ruoyi.chat.ab.mapper.ChatFileDataMapper;
import com.ruoyi.chat.ab.service.IChatFileDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文件与请求地址 服务实现类
 * </p>
 *
 * @author mataoxun
 * @since 2021-10-11
 */
@Service
public class ChatFileDataServiceImpl extends ServiceImpl<ChatFileDataMapper, ChatFileData> implements IChatFileDataService {

}
