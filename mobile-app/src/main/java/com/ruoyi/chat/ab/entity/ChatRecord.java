package com.ruoyi.chat.ab.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 聊天记录 
 * </p>
 *
 * @author mataoxun
 * @since 2021-10-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("chat_record")
public class ChatRecord implements Serializable {

    public static final Integer MSG_TYPE_TEXT = 1;
    public static final Integer MSG_TYPE_IMAGE = 2;
    public static final Integer MSG_TYPE_VIDEO = 3;
    public static final Integer MSG_TYPE_DOCUMENT = 4;

    public static ChatRecord ChatRecordUnRead(String senderId, String receiverId, String message, Integer msgType) {
        ChatRecord chatrecord = new ChatRecord();
        chatrecord.setIfRead("F").setMessage(message).setSenderId(senderId).setReceiverId(receiverId).setMsgType(msgType);
        return chatrecord;
    }
    public static ChatRecord ChatRecordReaded(String senderId, String receiverId, String message, Integer msgType) {
        ChatRecord chatrecord = new ChatRecord();
        chatrecord.setIfRead("T").setMessage(message).setSenderId(senderId).setReceiverId(receiverId).setMsgType(msgType);
        return chatrecord;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */

    @TableField(value = "CREATED_TIME", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createdTime;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updatedTime;

    /**
     * 发送方id
     */
    @TableField("sender_id")
    private String senderId;

    /**
     * 接收方id
     */
    @TableField("receiver_id")
    private String receiverId;

    /**
     * 消息
     */
    @TableField("message")
    private String message;

    /**
     * 消息类型
     */
    @TableField("msg_type")
    private Integer msgType;

    /**
     * 是否已推送过
     */
    @TableField("if_read")
    private String ifRead;

    /**
     * 撤回
     */
    @TableField(value = "rb",fill = FieldFill.INSERT)
    private String rb;

    /**
     * 逻辑删除
     */
    @TableField(value = "deleted",fill = FieldFill.INSERT)
    @TableLogic(value = "F",delval = "T")
    private String deleted;

    @TableId(value = "msg_id", type = IdType.ASSIGN_UUID)
    private String msgId;


}
