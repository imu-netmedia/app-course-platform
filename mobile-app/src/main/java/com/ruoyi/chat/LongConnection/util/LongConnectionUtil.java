package com.ruoyi.chat.LongConnection.util;

import com.ruoyi.chat.LongConnection.model.ReturnMsgVo;
import io.netty.channel.socket.SocketChannel;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @package: com.ruoyi.chat.LongConnection.util
 * @className: LongConnectionUtil
 * @description: 连接管理工具类
 * @author MaTaoxun
 * @date 2021/9/28 22:52
*/
public class LongConnectionUtil {
    public static ConcurrentHashMap<String, SocketChannel> idToChannelMap;
    public static ConcurrentHashMap<SocketChannel, String> channelToIdMap;

    static {
        idToChannelMap = new ConcurrentHashMap<>();
        channelToIdMap = new ConcurrentHashMap<>();
    }

    /**
    * @description: 通过通道查看是否在线
    * @param ch 通道
    * @return bool 是否在线
    * @author MaTaoxun
    * @date 2021/9/28 22:53
    */
    public static boolean checkChannelWhetherOnline(SocketChannel ch) {
        return channelToIdMap.containsKey(ch);
    }



    /**
    * @description: 通过用户id检查是否在线
    * @param id 用户id
    * @return bool 是否在线
    * @author MaTaoxun
    * @date 2021/9/28 22:54
    */
    public static boolean checkIdWhetherOnline(String id) {
        return idToChannelMap.containsKey(id);
    }

    /**
    * @description: 添加一个新用户，如果该id已经在线，就顶掉
    * @param id 用户id
    * @param ch 通道
    * @return bool 是否成功
    * @author MaTaoxun
    * @date 2021/9/28 22:53
    */
    public static boolean add(String id, SocketChannel ch) {
        try {
            // 检查是否已经在线，已经在线就踢掉
            if (idToChannelMap.containsKey(id)){
                SocketChannel channel = idToChannelMap.get(id);
                removedById(id);
                channel.writeAndFlush(LongConnectionMsgUtil.ObjectToWSTextResult(new ReturnMsgVo(ReturnMsgVo.KICK_OFF,"您已经被踢出")));
                channel.close();
            }
            channelToIdMap.put(ch, id);
            idToChannelMap.put(id, ch);
//            System.out.println(idToChannelMap);
//            System.out.println(channelToIdMap);
            return true;
        } catch (Exception e) {
            //System.out.println("LongConnectionUtil.add:"+e.getMessage());
            return false;
        }
    }

    /**
    * @author MaTaoxun
    * @date 2021/9/28 22:56
    */
    public static SocketChannel getChannelById(String id) {
        return idToChannelMap.get(id);
    }

    /**
    * @author MaTaoxun
    * @date 2021/9/28 22:56
    */
    public static String getIdByChannel(SocketChannel channel) {
        return channelToIdMap.get(channel);
    }

    /**
    * @author MaTaoxun
    * @date 2021/9/28 22:56
    */
    public static boolean removedByChannel(SocketChannel channel) {
        try {
            idToChannelMap.remove(channelToIdMap.get(channel));
            channelToIdMap.remove(channel);
            return true;
        } catch (Exception e) {
            //System.out.println("removedByChannel出错");
            return false;
        }
    }

    /**
    * @author MaTaoxun
    * @date 2021/9/28 22:56
    */
    public static boolean removedById(String id) {
        try {
            channelToIdMap.remove(idToChannelMap.get(id));
            idToChannelMap.remove(id);
            return true;
        } catch (Exception e) {
            //System.out.println("removedById出错");
            return false;
        }
    }
}
