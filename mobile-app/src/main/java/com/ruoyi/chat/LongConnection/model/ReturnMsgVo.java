package com.ruoyi.chat.LongConnection.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @package: com.ruoyi.chat.LongConnection.model
 * @className: ReturnMsgVo
 * @description: 长连接服务返沪信息类
 * @author MaTaoxun
 * @date 2021/9/28 22:58
*/
@Data
@AllArgsConstructor
public class ReturnMsgVo {
    //成功登录
    public static final Integer LOG_SUCCESS = 1;
    //数据包错误
    public static final Integer WRONG_PACKAGE = 2;
    //未登录
    public static final Integer WONT_LOG = 3;
    //成功发送
    public static final Integer SEND_SUCCESS = 4;
    //发送失败
    public static final Integer SEND_FAIL = 5;
    //踢出
    public static final Integer KICK_OFF = 6;
    //返回信息
    public static final Integer RECEIVE_MSG = 7;
    private Integer status;
    private Object message;
}
