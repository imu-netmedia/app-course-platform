package com.ruoyi.chat.LongConnection.handler.nettyService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ruoyi.chat.LongConnection.model.LongConnectionChatDTO;
import io.netty.channel.ChannelHandlerContext;

@FunctionalInterface
public interface IChatHandle {
    void handle(ChannelHandlerContext ctx, LongConnectionChatDTO chatDTO, String senderId) throws Exception;
}
