package com.ruoyi.chat.LongConnection.model.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileMsg {
    private String fileName;
    private byte[] stream;
}
