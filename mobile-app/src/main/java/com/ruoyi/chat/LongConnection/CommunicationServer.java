package com.ruoyi.chat.LongConnection;

import com.ruoyi.chat.LongConnection.Initializer.ChatChannelInitializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @package: com.ruoyi.chat.LongConnection
 * @className: CommunicationServer
 * @description: netty服务启动类
 * @author MaTaoxun
 * @date 2021/9/28 22:51
*/
@Component
public class CommunicationServer {

    @Resource
    private ChatChannelInitializer chatChannelInitializer;

    public void  run(int port) throws Exception {
        System.out.println("聊天服务启动成功端口为:"+port);
        EventLoopGroup bossGroup = new NioEventLoopGroup(2);
        EventLoopGroup workerGroup = new NioEventLoopGroup(15);
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childHandler(chatChannelInitializer);
            ChannelFuture f = bootstrap.bind(port).sync();
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}
