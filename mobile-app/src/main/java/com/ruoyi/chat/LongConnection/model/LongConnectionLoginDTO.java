package com.ruoyi.chat.LongConnection.model;

import lombok.Data;

/**
 * @package: com.ruoyi.chat.LongConnection.model
 * @className: LongConnectionLoginDTO
 * @description: 登录的DTO
 * @author MaTaoxun
 * @date 2021/9/30 20:20
*/
@Data
public class LongConnectionLoginDTO {
    private String senderId;
}
