package com.ruoyi.chat.LongConnection.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @package: com.ruoyi.chat.LongConnection.model
 * @className: SendMsgVo
 * @description: 给别人发信息返回的信息类
 * @author MaTaoxun
 * @date 2021/10/10 17:36
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendMsgVo {
    private String chatId;
    private Object content;
}
