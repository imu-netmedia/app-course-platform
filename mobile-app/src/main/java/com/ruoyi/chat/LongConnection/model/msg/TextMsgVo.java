package com.ruoyi.chat.LongConnection.model.msg;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.github.dozermapper.core.Mapping;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TextMsgVo {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Mapping("createdTime")
    private LocalDateTime time;


    /**
     * 发送方id
     */

    private String senderId;

    /**
     * 接收方id
     */

    private String receiverId;

    /**
     * 消息
     */

    private String message;


    /**
     * 撤回
     */

    private String rb;


    private String msgId;

}
