package com.ruoyi.chat.LongConnection.model;

import lombok.Data;

/**
 * @package: com.ruoyi.chat.LongConnection.model.LongConnectionChatDTO.java
 * @className: LongConnectionChatDTO
 * @description: 向其他人单聊发送信息的DTO
 * @author MaTaoxun
 * @date 2021/9/28 22:41
*/
@Data
public class LongConnectionChatDTO {
    public final static Integer FILE = 5;
    public final static Integer VIDEO = 4;
    public final static Integer SOUND = 3;
    public final static Integer IMAGE = 2;
    public final static Integer TEXT = 1;

    private String chatId;
    private String receiverId;
    private Object message;
    private Integer msgType;
}
