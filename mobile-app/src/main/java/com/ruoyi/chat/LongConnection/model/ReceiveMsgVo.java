package com.ruoyi.chat.LongConnection.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @package: com.ruoyi.chat.LongConnection.model
 * @className: ReceiveMsgVo
 * @description: 有人给该用户发信息返回的信息类
 * @author MaTaoxun
 * @date 2021/9/28 22:59
*/
@Data
@AllArgsConstructor
public class ReceiveMsgVo {
    private Integer msgType;
    private Object message;
}
