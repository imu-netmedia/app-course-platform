package com.ruoyi.chat.LongConnection.model.msg;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.github.dozermapper.core.Mapping;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @package: com.ruoyi.chat.LongConnection.model.msg
 * @className: FileMsgVo
 * @description: 返回filemsg的类
 * @author MaTaoxun
 * @date 2021/10/10 15:20
*/
@Data
@AllArgsConstructor
@Accessors(chain = true)
@NoArgsConstructor
public class FileMsgVo {
    /**
     * 创建时间
     */


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Mapping("createdTime")
    private LocalDateTime time;



    /**
     * 发送方id
     */

    private String senderId;

    /**
     * 接收方id
     */

    private String receiverId;


    /**
     * 撤回
     */

    private String rb;


    private String msgId;
    private String fileName;
    private String url;
}
