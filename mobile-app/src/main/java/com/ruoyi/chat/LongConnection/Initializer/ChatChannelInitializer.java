package com.ruoyi.chat.LongConnection.Initializer;

import com.ruoyi.chat.LongConnection.handler.ChatHandler;
import com.ruoyi.chat.LongConnection.handler.LogHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import org.springframework.stereotype.Component;

/**
 * @package: com.ruoyi.chat.LongConnection.Initializer
 * @className: ChatChannelInitializer
 * @description: 长连接服务Initializer
 * @author MaTaoxun
 * @date 2021/9/28 22:59
*/
@Component
public class ChatChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();// websocket 基于http协议，所以要有http编解码器
        pipeline.addLast("logging",new LoggingHandler("DEBUG"))
                .addLast(new HttpServerCodec())
                .addLast(new HttpObjectAggregator(1024*64))
                .addLast(new ChunkedWriteHandler())
                .addLast(new WebSocketServerProtocolHandler("/ws", null, false, 1024*64*1024, false, false, 10000L))
                .addLast(new LogHandler())
                .addLast(new ChatHandler());


    }
}
