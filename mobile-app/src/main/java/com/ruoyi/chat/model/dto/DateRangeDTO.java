package com.ruoyi.chat.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * @package: com.ruoyi.chat.model.dto
 * @className: DateRangeDTO
 * @description: 描述日期范围以查找聊天记录的DTO
 * @author MaTaoxun
 * @date 2021/10/2 19:30
*/
@Data
public class DateRangeDTO {
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime beginTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;
    private String id1;
    private String id2;
}
