package com.ruoyi.chat.model.vo;

import com.github.dozermapper.core.Mapping;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Comparator;

/**
 * @package: com.ruoyi.chat.model.vo
 * @className: MessageVo
 * @description: 返回的信息实体类
 * @author MaTaoxun
 * @date 2021/10/2 19:31
*/
@Data
public class MessageVo {

    private String receiverId;
    private String senderId;
    private String message;
    private Integer msgType;
    private String msgId;
    @Mapping("rb")
    private String rollback;
    @Mapping("createdTime")
    private LocalDateTime sendTime;

    /**
     * @package: com.ruoyi.chat.model.vo
     * @className: MessageVo
     * @description: 信息实体类排序
     * @author MaTaoxun
     * @date 2021/10/2 19:31
    */
    public static class MessageVoCompare implements Comparator<MessageVo>{

        @Override
        public int compare(MessageVo o1, MessageVo o2) {
            return o1.getSendTime().isAfter(o2.getSendTime()) ? -1 : 1;
        }
    }



}
