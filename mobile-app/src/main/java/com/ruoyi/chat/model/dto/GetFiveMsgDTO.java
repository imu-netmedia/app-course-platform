package com.ruoyi.chat.model.dto;

import lombok.Data;

/**
 * @package: com.ruoyi.chat.model.dto
 * @className: GetFiveMsgDTO
 * @description: 获取五条聊天记录的DTO
 * @author MaTaoxun
 * @date 2021/10/2 19:31
*/
@Data
public class GetFiveMsgDTO {
    private String Id1;
    private String Id2;
    private String msgId;
}
