package com.ruoyi.data.controller;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.data.filter.first.AccessTokenService;
import com.ruoyi.common.core.domain.entity.data.AccessBody;
import com.ruoyi.data.entity.DataApp;
import com.ruoyi.data.entity.DataKey;
import com.ruoyi.data.service.IDataAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/data/app")
public class DataAppController extends BaseController {

    @Autowired
    private IDataAppService dataAppService;

    @Autowired
    private AccessTokenService accessTokenService;


    /**
     * 获取应用列表
     */
    @GetMapping("/list")
    public TableDataInfo list(DataApp app)
    {

        if(getUserId() != 1){
            app.setUserId(getUserId());
        }

        startPage();
        List<DataApp> list = dataAppService.selectAppList(app);
        return getDataTable(list);
    }

//    /**
//     *
//     * @param appId
//     * @param userId
//     * @return
//     */
//    @GetMapping("/getAccessToken")
//    public AjaxResult getAccessToken(@RequestParam(required = false)String appId,@RequestParam(required = false)Long userId){
//        if(userId == null){
//            userId = getUserId();
//        }
//        AjaxResult ajax = new AjaxResult();
//        if(appId == null){
//            return AjaxResult.error(301,"请先申请key值");
//        }else {
//            String appKey = dataAppService.checkByAppKeyAndAppId(new DataKey(Long.valueOf(appId),userId));
//            //判断key值是否正确
//            if(appKey != null){
//                String accessToken = accessTokenService.createAccessToken(new AccessBody(Long.valueOf(appId),userId,appKey));
//                ajax.put("accessToken",accessToken);
//            }else {
//                return AjaxResult.error(302,"请输入正确的key值");
//            }
//
//        }
//        return  ajax;
//    }


    /**
     * 用户申请key
     */
    @PostMapping
    public AjaxResult getKey(@Validated @RequestBody DataApp dataApp){
        String appKey = UUID.randomUUID().toString();
        dataApp.setAppKey(appKey);
        dataApp.setUserId(getUserId());


        if (dataAppService.checkAppNameUnique(dataApp.getAppName()) != null)
        {
            return AjaxResult.error("应用名称" + dataApp.getAppName() + "'失败，应用名称已存在'");
        }

        dataApp.setCreateBy(getUsername());

        return toAjax(dataAppService.insertKey(dataApp));

    }

    /**
     * 根据用户编号获取详细信息
     */
    @GetMapping(value = {"/{id}" })
    public AjaxResult getInfo(@PathVariable(value = "id", required = false) Long id)
    {

        DataApp dataApp = dataAppService.selectById(id);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("dataApp",dataApp);
        return ajax;
    }

//    /**
//     * 新增应用
//     */
//    @Log(title = "应用管理", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@Validated @RequestBody DataApp dataApp)
//    {
//        if (dataAppService.checkAppNameUnique(dataApp.getAppName()) != null)
//        {
//            return AjaxResult.error("新增应用" + dataApp.getAppName() + "'失败，应用名称已存在'");
//        }
//        else if (dataAppService.checkUrlUnique(dataApp.getUrl()) != null)
//        {
//            return AjaxResult.error("新增应用'" + dataApp.getUrl() + "'失败，地址已存在");
//        }
//        dataApp.setCreateBy(getUsername());
//        return toAjax(dataAppService.insert(dataApp));
//    }
//
    /**
     * 修改应用
     */
    @Log(title = "应用管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody DataApp dataApp)
    {

        dataApp.setUpdateBy(getUsername());
        return toAjax(dataAppService.updataApp(dataApp));
    }

    /**
     * 删除应用
     */
    @Log(title = "应用管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {

        return toAjax(dataAppService.deleteAppById(ids));
    }

    @RequestMapping("/test")
    public void getAppKey(){
        System.out.println(dataAppService.selectAppKeyByAppId(1l));
    }

}
