package com.ruoyi.data.mapper;

import com.ruoyi.data.entity.DataApp;
import com.ruoyi.data.entity.DataKey;

import java.util.List;

public interface DataAppMapper {

    /**
     * 根据条件分页查询应用列表
     * @param app
     * @return
     */
    public List<DataApp> selectAppList(DataApp app);

    /**
     * 用户申请应用key值
     * @param dataApp
     * @return
     */
    public int insertKey(DataApp dataApp);


    /**
     * 通过ID进行查询
     * @param id
     * @return
     */
    public DataApp selectById(Long id);

    /**
     * 校验用户名称是否唯一
     *
     * @param appName 应用名称
     * @return 结果
     */
    public DataApp checkAppNameUnique(String appName);

    /**
     * 新增应用信息
     * @param dataApp
     * @return
     */
    public int insert(DataApp dataApp);


    /**
     * 修改信息
     * @param dataApp
     * @return
     */
    public int updataApp(DataApp dataApp);

    /**
     *
     * @param ids
     * @return
     */
    public int deleteAppById(Long[] ids);


    /**
     *
     * @param userId
     * @return
     */
    public List<DataKey> selectAppsByUserId(Long userId);


    /**
     * 判断输入的key值是否正确
     * @param dataKey
     * @return
     */
    public List<DataKey> checkByAppKeyAndAppId(DataKey dataKey);

    /**
     * 通过appId获取appKey
     * @param appId
     * @return
     */
    public String selectAppKeyByAppId(Long appId);
}
