package com.ruoyi.data.service;


import com.ruoyi.data.entity.DataApp;
import com.ruoyi.data.entity.DataKey;

import java.util.List;
import java.util.Map;

/**
 *
 * 应用管理业务层
 *
 */
public interface IDataAppService {

    /**
     * 根据条件分页查询应用列表
     * @param app
     * @return
     */
    public List<DataApp> selectAppList(DataApp app);

    /**
     * 用户申请应用key值
     * @param dataApp
     * @return
     */
    public int insertKey(DataApp dataApp);


    /**
     * 通过ID进行查询
     * @param id
     * @return
     */
    public DataApp selectById(Long id);

    /**
     * 校验用户名称是否唯一
     *
     * @param appName 应用名称
     * @return 结果
     */
    public DataApp checkAppNameUnique(String appName);

    /**
     * 新增应用信息
     * @param dataApp
     * @return
     */
    public int insert(DataApp dataApp);


    /**
     * 修改用户信息
     * @param dataApp
     * @return
     */
    public int updataApp(DataApp dataApp);

    /**
     *
     * @param ids
     * @return
     */
    public int deleteAppById(Long[] ids);


    /**
     * 获取用户的
     * @param userId
     * @return
     */
    public Map<String,Map<String,String>> selectAppsByUserId(Long userId);

    /**
     * 判断输入的key值是否正确
     * @param dataKey
     * @return
     */
    public String checkByAppKeyAndAppId(DataKey dataKey);

    /**
     * 通过appId获取appKey
     * @param appId
     * @return
     */
    public String selectAppKeyByAppId(Long appId);

}
