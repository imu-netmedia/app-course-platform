package com.ruoyi.data.service.impl;


import com.ruoyi.data.entity.DataApp;
import com.ruoyi.data.entity.DataKey;
import com.ruoyi.data.mapper.DataAppMapper;
import com.ruoyi.data.service.IDataAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataAppServiceImpl implements IDataAppService {

    @Autowired
    private DataAppMapper dataAppMapper;

    @Override
    public List<DataApp> selectAppList(DataApp app) {
        return dataAppMapper.selectAppList(app);
    }

    /**
     * 用户申请应用key值
     * @param dataApp
     * @return
     */
    @Override
    public int insertKey(DataApp dataApp){
        return dataAppMapper.insertKey(dataApp);
    }


    @Override
    public DataApp selectById(Long id) {
        return dataAppMapper.selectById(id);
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param appName 应用名称
     * @return 结果
     */
    public DataApp checkAppNameUnique(String appName){
        return dataAppMapper.checkAppNameUnique(appName);
    }


    /**
     * 新增应用信息
     * @param dataApp
     * @return
     */
    public int insert(DataApp dataApp){
        return dataAppMapper.insert(dataApp);
    }


    /**
     * 修改用户信息
     * @param dataApp
     * @return
     */
    public int updataApp(DataApp dataApp){
        return dataAppMapper.updataApp(dataApp);
    }

    /**
     *
     * @param ids
     * @return
     */
    public int deleteAppById(Long[] ids){

        return dataAppMapper.deleteAppById(ids);
    }

    @Override
    public Map<String,Map<String,String>> selectAppsByUserId(Long userId) {

        Map<String,Map<String,String>> map = new HashMap<>();

        List<DataKey>list  = dataAppMapper.selectAppsByUserId(userId);

        for( DataKey dataKey : list){
            Map<String ,String> map1 = new HashMap<>();
            map1.put("key",dataKey.getAppId().toString());
            map1.put("value",dataKey.getAppKey());
            map.put(dataKey.getUrl(),map1);
        }
        return map;
    }

    @Override
    public String checkByAppKeyAndAppId(DataKey dataKey) {
        return dataAppMapper.checkByAppKeyAndAppId(dataKey).get(0).getAppKey();
    }

    @Override
    public String selectAppKeyByAppId(Long appId) {
        return dataAppMapper.selectAppKeyByAppId(appId);
    }
}
