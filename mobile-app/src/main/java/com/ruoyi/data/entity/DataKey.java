package com.ruoyi.data.entity;

import com.ruoyi.common.core.domain.BaseEntity;


/**
 *  用户 申请 应用的key值
 */
public class DataKey extends BaseEntity {

    private static final long serialVersionUID = 1L;


    private Long id;

    private Long appId;

    private Long userId;

    private String appKey;

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public DataKey(Long appId, Long userId) {
        this.appId = appId;
        this.userId = userId;
    }

    public DataKey() {
    }

    public DataKey(Long appId, Long userId, String appKey) {
        this.appId = appId;
        this.userId = userId;
        this.appKey = appKey;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
