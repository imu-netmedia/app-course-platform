package com.ruoyi.data.entity;


import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 数据中心 应用管理
 */
public class DataApp extends BaseEntity {


    private static final long serialVersionUID = 1L;

    //主键
    private Long id;
    //用户名称
    private String nickName;
    //应用id（一个用户一个唯一的）
    private Long appId;
    //应用名称
    private String appName;
    //应用简介
    private String text;
    //用户key值
    private String appKey;
    //用户id
    private Long userId;
    //状态吗 (是否被申请)
    private int status;



    public DataApp(Long userId, String appKey) {
        this.userId = userId;
        this.appKey = appKey;
    }

    public DataApp(Long appId, String appKey, Long userId) {
        this.appId = appId;
        this.appKey = appKey;
        this.userId = userId;
    }

    public DataApp() {
    }

    public DataApp(Long appId, String appName, String text, String appKey, Long userId, int status) {
        this.appId = appId;
        this.appName = appName;
        this.text = text;
        this.appKey = appKey;
        this.userId = userId;
        this.status = status;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
