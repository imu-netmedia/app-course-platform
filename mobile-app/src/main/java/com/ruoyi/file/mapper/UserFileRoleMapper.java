package com.ruoyi.file.mapper;

import java.util.List;
import com.ruoyi.file.domain.UserFileRole;
import org.apache.ibatis.annotations.Param;

/**
 * 用户文档权限Mapper接口
 * 
 * @author zhangxiliang
 * @date 2021-11-04
 */
public interface UserFileRoleMapper 
{
    /**
     * 查询用户文档权限
     * 
     * @param roleId 用户文档权限主键
     * @return 用户文档权限
     */
    public UserFileRole selectUserFileRoleByRoleId(Long roleId);

    /**
     * 查询用户文档权限列表
     * 
     * @param userFileRole 用户文档权限
     * @return 用户文档权限集合
     */
    public List<UserFileRole> selectUserFileRoleList(UserFileRole userFileRole);

    /**
     * 新增用户文档权限
     * 
     * @param userFileRole 用户文档权限
     * @return 结果
     */
    public int insertUserFileRole(UserFileRole userFileRole);

    /**
     * 批量添加用户文档权限
     * @param fileId
     * @param userIds
     * @return 结果
     */
    public int insertUserFileRoles(@Param("fileId") Long fileId, @Param("userIds") Long[] userIds);

    /**
     * 修改用户文档权限
     * 
     * @param userFileRole 用户文档权限
     * @return 结果
     */
    public int updateUserFileRole(UserFileRole userFileRole);

    /**
     * 删除用户文档权限
     * 
     * @param roleId 用户文档权限主键
     * @return 结果
     */
    public int deleteUserFileRoleByRoleId(Long roleId);

    /**
     * 批量删除用户文档权限
     * 
     * @param roleIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserFileRoleByRoleIds(Long[] roleIds);
}
