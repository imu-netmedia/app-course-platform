package com.ruoyi.file.mapper;

import com.ruoyi.file.domain.UserFileInfo;
import com.ruoyi.file.domain.UserFileRole;
import com.ruoyi.file.domain.vo.GetFileVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文件信息 Api Mapper接口
 * 
 * @author zhangxiliang
 * @date 2021-10-31
 */
public interface UserFileInfoApiMapper
{
    /**
     * 查询文件信息
     * 
     * @param fileId 文件信息主键
     * @return 文件信息
     */
    public UserFileInfo selectUserFileInfoByFileId(Long fileId);

    /**
     * 查询文件信息列表
     * @param getFileVo
     * @return 文件信息集合
     */
    public List<UserFileInfo> selectUserFileInfoList(GetFileVo getFileVo);

    /**
     * 新增文件信息
     * 
     * @param userFileInfo 文件信息
     * @return 结果
     */
    public int insertUserFileInfo(UserFileInfo userFileInfo);

    /**
     * 修改文件信息
     * 
     * @param userFileInfo 文件信息
     * @return 结果
     */
    public int updateUserFileInfo(UserFileInfo userFileInfo);

    /**
     * 删除文件信息
     * 
     * @param fileId 文件信息主键
     * @return 结果
     */
    public int deleteUserFileInfoByFileId(Long fileId);

    /**
     * 批量删除文件信息
     * 
     * @param fileIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserFileInfoByFileIds(Long[] fileIds);


}
