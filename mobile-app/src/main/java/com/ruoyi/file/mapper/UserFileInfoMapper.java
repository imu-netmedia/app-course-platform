package com.ruoyi.file.mapper;

import java.util.List;
import com.ruoyi.file.domain.UserFileInfo;
import com.ruoyi.file.domain.UserFileRole;

/**
 * 文件信息Mapper接口
 * 
 * @author zhangxiliang
 * @date 2021-10-31
 */
public interface UserFileInfoMapper 
{
    /**
     * 查询文件信息
     * 
     * @param fileId 文件信息主键
     * @return 文件信息
     */
    public UserFileInfo selectUserFileInfoByFileId(Long fileId);

    /**
     * 查询文件信息列表
     * 
     * @param userFileInfo 文件信息
     * @return 文件信息集合
     */
    public List<UserFileInfo> selectUserFileInfoList(UserFileInfo userFileInfo);

    /**
     * 新增文件信息
     * 
     * @param userFileInfo 文件信息
     * @return 结果
     */
    public int insertUserFileInfo(UserFileInfo userFileInfo);

    /**
     * 修改文件信息
     * 
     * @param userFileInfo 文件信息
     * @return 结果
     */
    public int updateUserFileInfo(UserFileInfo userFileInfo);

    /**
     * 删除文件信息
     * 
     * @param fileId 文件信息主键
     * @return 结果
     */
    public int deleteUserFileInfoByFileId(Long fileId);

    /**
     * 批量删除文件信息
     * 
     * @param fileIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserFileInfoByFileIds(Long[] fileIds);

    /**
     * 批量删除用户文件权限
     * 
     * @param fileIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserFileRoleByFileIds(Long[] fileIds);
    
    /**
     * 批量新增用户文件权限
     * 
     * @param userFileRoleList 用户文件权限列表
     * @return 结果
     */
    public int batchUserFileRole(List<UserFileRole> userFileRoleList);
    

    /**
     * 通过文件信息主键删除用户文件权限信息
     * 
     * @param fileId 文件信息ID
     * @return 结果
     */
    public int deleteUserFileRoleByFileId(Long fileId);
}
