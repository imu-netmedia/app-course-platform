package com.ruoyi.file.mapper;

import java.util.List;
import com.ruoyi.file.domain.ClassesFileRole;
import org.apache.ibatis.annotations.Param;

/**
 * 班级文档权限Mapper接口
 * 
 * @author zhangxiliang
 * @date 2021-11-04
 */
public interface ClassesFileRoleMapper 
{
    /**
     * 查询班级文档权限
     * 
     * @param roleId 班级文档权限主键
     * @return 班级文档权限
     */
    public ClassesFileRole selectClassesFileRoleByRoleId(Long roleId);

    /**
     * 查询班级文档权限列表
     * 
     * @param classesFileRole 班级文档权限
     * @return 班级文档权限集合
     */
    public List<ClassesFileRole> selectClassesFileRoleList(ClassesFileRole classesFileRole);

    /**
     * 新增班级文档权限
     * 
     * @param classesFileRole 班级文档权限
     * @return 结果
     */
    public int insertClassesFileRole(ClassesFileRole classesFileRole);

    /**
     * 批量添加班级文档权限
     * @param fileId
     * @param classesIds
     * @return 结果
     */
    public int insertClassesFileRoles(@Param("fileId") Long fileId , @Param("classesIds") Long[] classesIds);

    /**
     * 修改班级文档权限
     * 
     * @param classesFileRole 班级文档权限
     * @return 结果
     */
    public int updateClassesFileRole(ClassesFileRole classesFileRole);

    /**
     * 删除班级文档权限
     * 
     * @param roleId 班级文档权限主键
     * @return 结果
     */
    public int deleteClassesFileRoleByRoleId(Long roleId);

    /**
     * 批量删除班级文档权限
     * 
     * @param roleIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteClassesFileRoleByRoleIds(Long[] roleIds);
}
