package com.ruoyi.file.domain;

import com.ruoyi.eduinfo.model.Contacts;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户文档权限对象 user_file_role
 * 
 * @author zhangxiliang
 * @date 2021-11-04
 */
public class UserFileRole extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long roleId;

    /** 文件id */
    @Excel(name = "文件id")
    private Long fileId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 学号 */
    private String userName;

    /** 联系人实体 */
    private Contacts contacts;

    public void setRoleId(Long roleId) 
    {
        this.roleId = roleId;
    }

    public Long getRoleId() 
    {
        return roleId;
    }
    public void setFileId(Long fileId) 
    {
        this.fileId = fileId;
    }

    public Long getFileId() 
    {
        return fileId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "UserFileRole{" +
                "roleId=" + roleId +
                ", fileId=" + fileId +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", contacts=" + contacts +
                '}';
    }
}
