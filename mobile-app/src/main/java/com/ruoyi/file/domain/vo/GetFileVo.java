package com.ruoyi.file.domain.vo;

import lombok.Data;

/**
 * 获取文件列表参数Vo
 * @author Zhangxiliang
 * @date 2021-11-06
 */

@Data
public class GetFileVo {
    private Long createBy;
    private Long classesId;
    private Long userId;
}
