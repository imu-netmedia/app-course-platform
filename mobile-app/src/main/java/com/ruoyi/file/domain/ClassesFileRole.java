package com.ruoyi.file.domain;

import com.ruoyi.eduinfo.model.Classes;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 班级文档权限对象 classes_file_role
 * 
 * @author zhangxiliang
 * @date 2021-11-04
 */
public class ClassesFileRole extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long roleId;

    /** 文件id */
    @Excel(name = "文件id")
    private Long fileId;

    /** 班级id */
    @Excel(name = "班级id")
    private Long classesId;

    /** 班级对象 */
    private Classes classes;

    public void setRoleId(Long roleId) 
    {
        this.roleId = roleId;
    }

    public Long getRoleId() 
    {
        return roleId;
    }
    public void setFileId(Long fileId) 
    {
        this.fileId = fileId;
    }

    public Long getFileId() 
    {
        return fileId;
    }
    public void setClassesId(Long classesId) 
    {
        this.classesId = classesId;
    }

    public Long getClassesId() 
    {
        return classesId;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    @Override
    public String toString() {
        return "ClassesFileRole{" +
                "roleId=" + roleId +
                ", fileId=" + fileId +
                ", classesId=" + classesId +
                ", classes=" + classes +
                '}';
    }


}
