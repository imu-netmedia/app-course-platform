package com.ruoyi.file.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文件信息对象 user_file_info
 * 
 * @author zhangxiliang
 * @date 2021-10-31
 */
public class UserFileInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文件id（主键） */
    private Long fileId;

    /** 文件名称 */
    @Excel(name = "文件名称")
    private String fileName;

    /** 文件路径 */
    private String filePath;

    /** MIME文件类型 */
    @Excel(name = "MIME文件类型")
    private String fileMime;

    /** 文件下载次数 */
    @Excel(name = "文件下载次数")
    private Long fileDownload;

    /** 文件大小 */
    @Excel(name = "文件大小")
    private Long fileSize;

    /** 文件MD5值 */
    private String md5;

    /** 文件SHA1值 */
    private String sha1;

    /** 文件可见范围（0:全部可见,1:指定班级可见,2:指定人可见,3:指定人不可见,4:私有文件） */
    @Excel(name = "文件可见范围", readConverterExp = "0=:全部可见,1:指定班级可见,2:指定人可见,3:指定人不可见,4:私有文件")
    private Integer fileRange;

    /** 用户文件权限信息 */
    private List<UserFileRole> userFileRoleList;

    /** 班级文件权限信息 */
    private List<ClassesFileRole> classesFileRoleList;

    public void setFileId(Long fileId) 
    {
        this.fileId = fileId;
    }

    public Long getFileId() 
    {
        return fileId;
    }
    public void setFileName(String fileName) 
    {
        this.fileName = fileName;
    }

    public String getFileName() 
    {
        return fileName;
    }
    public void setFilePath(String filePath) 
    {
        this.filePath = filePath;
    }

    public String getFilePath() 
    {
        return filePath;
    }
    public void setFileMime(String fileMime) 
    {
        this.fileMime = fileMime;
    }

    public String getFileMime() 
    {
        return fileMime;
    }
    public void setFileDownload(Long fileDownload) 
    {
        this.fileDownload = fileDownload;
    }

    public Long getFileDownload() 
    {
        return fileDownload;
    }
    public void setFileSize(Long fileSize) 
    {
        this.fileSize = fileSize;
    }

    public Long getFileSize() 
    {
        return fileSize;
    }
    public void setMd5(String md5) 
    {
        this.md5 = md5;
    }

    public String getMd5() 
    {
        return md5;
    }
    public void setSha1(String sha1) 
    {
        this.sha1 = sha1;
    }

    public String getSha1() 
    {
        return sha1;
    }
    public void setFileRange(Integer fileRange) 
    {
        this.fileRange = fileRange;
    }

    public Integer getFileRange() 
    {
        return fileRange;
    }

    public List<UserFileRole> getUserFileRoleList()
    {
        return userFileRoleList;
    }
    public List<ClassesFileRole> getClassesFileRoleList()
    {
        return classesFileRoleList;
    }

    public void setUserFileRoleList(List<UserFileRole> userFileRoleList)
    {
        this.userFileRoleList = userFileRoleList;
    }


    public void setClassesFileRoleList(List<ClassesFileRole> classesFileRoleList)
    {
        this.classesFileRoleList = classesFileRoleList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("fileId", getFileId())
            .append("fileName", getFileName())
            .append("filePath", getFilePath())
            .append("fileMime", getFileMime())
            .append("fileDownload", getFileDownload())
            .append("fileSize", getFileSize())
            .append("md5", getMd5())
            .append("sha1", getSha1())
            .append("fileRange", getFileRange())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("userFileRoleList", getUserFileRoleList())
            .append("classesFileRoleList", getClassesFileRoleList())
            .toString();
    }
}
