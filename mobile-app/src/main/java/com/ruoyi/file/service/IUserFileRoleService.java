package com.ruoyi.file.service;

import java.util.List;
import com.ruoyi.file.domain.UserFileRole;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 用户文档权限Service接口
 * 
 * @author zhangxiliang
 * @date 2021-11-04
 */
public interface IUserFileRoleService 
{
    /**
     * 查询用户文档权限
     * 
     * @param roleId 用户文档权限主键
     * @return 用户文档权限
     */
    public UserFileRole selectUserFileRoleByRoleId(Long roleId);

    /**
     * 查询用户文档权限列表
     * 
     * @param userFileRole 用户文档权限
     * @return 用户文档权限集合
     */
    public List<UserFileRole> selectUserFileRoleList(UserFileRole userFileRole);

    /**
     * 新增用户文档权限
     * 
     * @param userFileRole 用户文档权限
     * @return 结果
     */
    public int insertUserFileRole(UserFileRole userFileRole);

    /**
     * 批量添加用户文档权限
     * @param fileId
     * @param userIds
     * @return 结果
     */
    public int insertUserFileRoles(Long fileId , Long[] userIds);

    /**
     * 修改用户文档权限
     * 
     * @param userFileRole 用户文档权限
     * @return 结果
     */
    public int updateUserFileRole(UserFileRole userFileRole);

    /**
     * 批量删除用户文档权限
     * 
     * @param roleIds 需要删除的用户文档权限主键集合
     * @return 结果
     */
    public int deleteUserFileRoleByRoleIds(Long[] roleIds);

    /**
     * 删除用户文档权限信息
     * 
     * @param roleId 用户文档权限主键
     * @return 结果
     */
    public int deleteUserFileRoleByRoleId(Long roleId);
}
