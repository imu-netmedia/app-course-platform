package com.ruoyi.file.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.file.domain.UserFileInfo;
import com.ruoyi.file.domain.UserFileRole;
import com.ruoyi.file.domain.vo.GetFileVo;
import com.ruoyi.file.mapper.UserFileInfoApiMapper;
import com.ruoyi.file.mapper.UserFileInfoMapper;
import com.ruoyi.file.service.IUserFileInfoApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件信息 Api Service业务层处理
 * 
 * @author zhangxiliang
 * @date 2021-10-31
 */
@Service
public class UserFileInfoApiServiceImpl implements IUserFileInfoApiService
{
    @Autowired
    private UserFileInfoApiMapper userFileInfoApiMapper;

    /**
     * 查询文件信息
     * 
     * @param fileId 文件信息主键
     * @return 文件信息
     */
    @Override
    public UserFileInfo selectUserFileInfoByFileId(Long fileId)
    {
        return userFileInfoApiMapper.selectUserFileInfoByFileId(fileId);
    }

    /**
     * 查询文件信息列表
     * @param getFileVo
     * @return 文件信息集合
     */
    @Override
    public List<UserFileInfo> selectUserFileInfoList(GetFileVo getFileVo)
    {
        return userFileInfoApiMapper.selectUserFileInfoList(getFileVo);
    }

    /**
     * 新增文件信息
     * 
     * @param userFileInfo 文件信息
     * @return 结果
     */
    @Transactional
    @Override
    public int insertUserFileInfo(UserFileInfo userFileInfo)
    {
        userFileInfo.setCreateTime(DateUtils.getNowDate());
        int rows = userFileInfoApiMapper.insertUserFileInfo(userFileInfo);
        return rows;
    }


    /**
     * 批量删除文件信息
     * 
     * @param fileIds 需要删除的文件信息主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteUserFileInfoByFileIds(Long[] fileIds)
    {
        return userFileInfoApiMapper.deleteUserFileInfoByFileIds(fileIds);
    }

    /**
     * 删除文件信息信息
     * 
     * @param fileId 文件信息主键
     * @return 结果
     */
    @Override
    public int deleteUserFileInfoByFileId(Long fileId)
    {
        return userFileInfoApiMapper.deleteUserFileInfoByFileId(fileId);
    }

}
