package com.ruoyi.file.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.file.mapper.UserFileRoleMapper;
import com.ruoyi.file.domain.UserFileRole;
import com.ruoyi.file.service.IUserFileRoleService;

/**
 * 用户文档权限Service业务层处理
 * 
 * @author zhangxiliang
 * @date 2021-11-04
 */
@Service
public class UserFileRoleServiceImpl implements IUserFileRoleService 
{
    @Autowired
    private UserFileRoleMapper userFileRoleMapper;

    /**
     * 查询用户文档权限
     * 
     * @param roleId 用户文档权限主键
     * @return 用户文档权限
     */
    @Override
    public UserFileRole selectUserFileRoleByRoleId(Long roleId)
    {
        return userFileRoleMapper.selectUserFileRoleByRoleId(roleId);
    }

    /**
     * 查询用户文档权限列表
     * 
     * @param userFileRole 用户文档权限
     * @return 用户文档权限
     */
    @Override
    public List<UserFileRole> selectUserFileRoleList(UserFileRole userFileRole)
    {
        return userFileRoleMapper.selectUserFileRoleList(userFileRole);
    }

    /**
     * 新增用户文档权限
     * 
     * @param userFileRole 用户文档权限
     * @return 结果
     */
    @Override
    public int insertUserFileRole(UserFileRole userFileRole)
    {
        return userFileRoleMapper.insertUserFileRole(userFileRole);
    }

    /**
     * 批量添加用户文档权限
     * @param fileId
     * @param userIds
     * @return 结果
     */
    @Override
    public int insertUserFileRoles(Long fileId, Long[] userIds) {

        return userFileRoleMapper.insertUserFileRoles(fileId,userIds);
    }

    /**
     * 修改用户文档权限
     * 
     * @param userFileRole 用户文档权限
     * @return 结果
     */
    @Override
    public int updateUserFileRole(UserFileRole userFileRole)
    {
        return userFileRoleMapper.updateUserFileRole(userFileRole);
    }

    /**
     * 批量删除用户文档权限
     * 
     * @param roleIds 需要删除的用户文档权限主键
     * @return 结果
     */
    @Override
    public int deleteUserFileRoleByRoleIds(Long[] roleIds)
    {
        return userFileRoleMapper.deleteUserFileRoleByRoleIds(roleIds);
    }

    /**
     * 删除用户文档权限信息
     * 
     * @param roleId 用户文档权限主键
     * @return 结果
     */
    @Override
    public int deleteUserFileRoleByRoleId(Long roleId)
    {
        return userFileRoleMapper.deleteUserFileRoleByRoleId(roleId);
    }
}
