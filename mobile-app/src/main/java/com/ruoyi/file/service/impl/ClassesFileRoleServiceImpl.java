package com.ruoyi.file.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.file.mapper.ClassesFileRoleMapper;
import com.ruoyi.file.domain.ClassesFileRole;
import com.ruoyi.file.service.IClassesFileRoleService;

/**
 * 班级文档权限Service业务层处理
 * 
 * @author zhangxiliang
 * @date 2021-11-04
 */
@Service
public class ClassesFileRoleServiceImpl implements IClassesFileRoleService 
{
    @Autowired
    private ClassesFileRoleMapper classesFileRoleMapper;

    /**
     * 查询班级文档权限
     * 
     * @param roleId 班级文档权限主键
     * @return 班级文档权限
     */
    @Override
    public ClassesFileRole selectClassesFileRoleByRoleId(Long roleId)
    {
        return classesFileRoleMapper.selectClassesFileRoleByRoleId(roleId);
    }

    /**
     * 查询班级文档权限列表
     * 
     * @param classesFileRole 班级文档权限
     * @return 班级文档权限
     */
    @Override
    public List<ClassesFileRole> selectClassesFileRoleList(ClassesFileRole classesFileRole)
    {
        return classesFileRoleMapper.selectClassesFileRoleList(classesFileRole);
    }

    /**
     * 新增班级文档权限
     * 
     * @param classesFileRole 班级文档权限
     * @return 结果
     */
    @Override
    public int insertClassesFileRole(ClassesFileRole classesFileRole)
    {
        return classesFileRoleMapper.insertClassesFileRole(classesFileRole);
    }

    /**
     * 批量添加班级文档权限
     * @param fileId
     * @param classesIds
     * @return 结果
     */
    @Override
    public int insertClassesFileRoles(Long fileId ,Long[] classesIds)
    {
        return classesFileRoleMapper.insertClassesFileRoles(fileId,classesIds);
    }

    /**
     * 修改班级文档权限
     * 
     * @param classesFileRole 班级文档权限
     * @return 结果
     */
    @Override
    public int updateClassesFileRole(ClassesFileRole classesFileRole)
    {
        return classesFileRoleMapper.updateClassesFileRole(classesFileRole);
    }

    /**
     * 批量删除班级文档权限
     * 
     * @param roleIds 需要删除的班级文档权限主键
     * @return 结果
     */
    @Override
    public int deleteClassesFileRoleByRoleIds(Long[] roleIds)
    {
        return classesFileRoleMapper.deleteClassesFileRoleByRoleIds(roleIds);
    }

    /**
     * 删除班级文档权限信息
     * 
     * @param roleId 班级文档权限主键
     * @return 结果
     */
    @Override
    public int deleteClassesFileRoleByRoleId(Long roleId)
    {
        return classesFileRoleMapper.deleteClassesFileRoleByRoleId(roleId);
    }
}
