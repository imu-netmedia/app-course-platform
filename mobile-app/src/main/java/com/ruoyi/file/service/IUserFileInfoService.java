package com.ruoyi.file.service;

import java.util.List;
import com.ruoyi.file.domain.UserFileInfo;

/**
 * 文件信息Service接口
 * 
 * @author zhangxiliang
 * @date 2021-10-31
 */
public interface IUserFileInfoService 
{
    /**
     * 查询文件信息
     * 
     * @param fileId 文件信息主键
     * @return 文件信息
     */
    public UserFileInfo selectUserFileInfoByFileId(Long fileId);

    /**
     * 查询文件信息列表
     *
     * @param userFileInfo 文件信息
     * @return 文件信息集合
     */
    public List<UserFileInfo> selectUserFileInfoList(UserFileInfo userFileInfo);

    /**
     * 新增文件信息
     * 
     * @param userFileInfo 文件信息
     * @return 结果
     */
    public int insertUserFileInfo(UserFileInfo userFileInfo);

    /**
     * 修改文件信息
     * 
     * @param userFileInfo 文件信息
     * @return 结果
     */
    public int updateUserFileInfo(UserFileInfo userFileInfo);

    /**
     * 批量删除文件信息
     * 
     * @param fileIds 需要删除的文件信息主键集合
     * @return 结果
     */
    public int deleteUserFileInfoByFileIds(Long[] fileIds);

    /**
     * 删除文件信息信息
     * 
     * @param fileId 文件信息主键
     * @return 结果
     */
    public int deleteUserFileInfoByFileId(Long fileId);
}
