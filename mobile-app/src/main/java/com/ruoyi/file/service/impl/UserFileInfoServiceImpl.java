package com.ruoyi.file.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.file.domain.UserFileRole;
import com.ruoyi.file.mapper.UserFileInfoMapper;
import com.ruoyi.file.domain.UserFileInfo;
import com.ruoyi.file.service.IUserFileInfoService;

/**
 * 文件信息Service业务层处理
 * 
 * @author zhangxiliang
 * @date 2021-10-31
 */
@Service
public class UserFileInfoServiceImpl implements IUserFileInfoService 
{
    @Autowired
    private UserFileInfoMapper userFileInfoMapper;

    /**
     * 查询文件信息
     * 
     * @param fileId 文件信息主键
     * @return 文件信息
     */
    @Override
    public UserFileInfo selectUserFileInfoByFileId(Long fileId)
    {
        return userFileInfoMapper.selectUserFileInfoByFileId(fileId);
    }

    /**
     * 查询文件信息列表
     * 
     * @param userFileInfo 文件信息
     * @return 文件信息
     */
    @Override
    public List<UserFileInfo> selectUserFileInfoList(UserFileInfo userFileInfo)
    {
        return userFileInfoMapper.selectUserFileInfoList(userFileInfo);
    }

    /**
     * 新增文件信息
     * 
     * @param userFileInfo 文件信息
     * @return 结果
     */
    @Transactional
    @Override
    public int insertUserFileInfo(UserFileInfo userFileInfo)
    {
        userFileInfo.setCreateTime(DateUtils.getNowDate());
        int rows = userFileInfoMapper.insertUserFileInfo(userFileInfo);
        return rows;
    }

    /**
     * 修改文件信息
     * 
     * @param userFileInfo 文件信息
     * @return 结果
     */
    @Transactional
    @Override
    public int updateUserFileInfo(UserFileInfo userFileInfo)
    {
        userFileInfoMapper.deleteUserFileRoleByFileId(userFileInfo.getFileId());
        return userFileInfoMapper.updateUserFileInfo(userFileInfo);
    }

    /**
     * 批量删除文件信息
     * 
     * @param fileIds 需要删除的文件信息主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteUserFileInfoByFileIds(Long[] fileIds)
    {
        userFileInfoMapper.deleteUserFileRoleByFileIds(fileIds);
        return userFileInfoMapper.deleteUserFileInfoByFileIds(fileIds);
    }

    /**
     * 删除文件信息信息
     * 
     * @param fileId 文件信息主键
     * @return 结果
     */
    @Override
    public int deleteUserFileInfoByFileId(Long fileId)
    {
        userFileInfoMapper.deleteUserFileRoleByFileId(fileId);
        return userFileInfoMapper.deleteUserFileInfoByFileId(fileId);
    }

}
