package com.ruoyi.file.controller;

import java.util.List;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.file.domain.UserFileInfo;
import com.ruoyi.file.service.IUserFileInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文件信息Controller
 * 
 * @author zhangxiliang
 * @date 2021-10-31
 */
@RestController
@RequestMapping("/file/info")
public class UserFileInfoController extends BaseController
{
    @Autowired
    private IUserFileInfoService userFileInfoService;
    /**
     * 查询文件信息列表
     */
    @PreAuthorize("@ss.hasPermi('file:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserFileInfo userFileInfo)
    {
        startPage();
        List<UserFileInfo> list = userFileInfoService.selectUserFileInfoList(userFileInfo);
        return getDataTable(list);
    }

    /**
     * 导出文件信息列表
     */
    @PreAuthorize("@ss.hasPermi('file:info:export')")
    @Log(title = "文件信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserFileInfo userFileInfo)
    {
        List<UserFileInfo> list = userFileInfoService.selectUserFileInfoList(userFileInfo);
        ExcelUtil<UserFileInfo> util = new ExcelUtil<UserFileInfo>(UserFileInfo.class);
        return util.exportExcel(list, "文件信息数据");
    }

    /**
     * 获取文件信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('file:info:query')")
    @GetMapping(value = "/{fileId}")
    public AjaxResult getInfo(@PathVariable("fileId") Long fileId)
    {
        return AjaxResult.success(userFileInfoService.selectUserFileInfoByFileId(fileId));
    }

    /**
     * 新增文件信息
     */
    @PreAuthorize("@ss.hasPermi('file:info:add')")
    @Log(title = "文件信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserFileInfo userFileInfo)
    {
        return toAjax(userFileInfoService.insertUserFileInfo(userFileInfo));
    }

    /**
     * 修改文件信息
     */
    @PreAuthorize("@ss.hasPermi('file:info:edit')")
    @Log(title = "文件信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserFileInfo userFileInfo)
    {
        return toAjax(userFileInfoService.updateUserFileInfo(userFileInfo));
    }

    /**
     * 删除文件信息
     */
    @PreAuthorize("@ss.hasPermi('file:info:remove')")
    @Log(title = "文件信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{fileIds}")
    public AjaxResult remove(@PathVariable Long[] fileIds)
    {
        return toAjax(userFileInfoService.deleteUserFileInfoByFileIds(fileIds));
    }
}
