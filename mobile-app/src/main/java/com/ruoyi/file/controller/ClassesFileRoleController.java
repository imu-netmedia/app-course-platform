package com.ruoyi.file.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.file.domain.ClassesFileRole;
import com.ruoyi.file.service.IClassesFileRoleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 班级文档权限Controller
 * 
 * @author zhangxiliang
 * @date 2021-11-04
 */
@RestController
@RequestMapping("/file/classesrole")
public class ClassesFileRoleController extends BaseController
{
    @Autowired
    private IClassesFileRoleService classesFileRoleService;

    /**
     * 查询班级文档权限列表
     */
    @PreAuthorize("@ss.hasPermi('file:classesrole:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClassesFileRole classesFileRole)
    {
        startPage();
        List<ClassesFileRole> list = classesFileRoleService.selectClassesFileRoleList(classesFileRole);
        return getDataTable(list);
    }

    /**
     * 导出班级文档权限列表
     */
    @PreAuthorize("@ss.hasPermi('file:classesrole:export')")
    @Log(title = "班级文档权限", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ClassesFileRole classesFileRole)
    {
        List<ClassesFileRole> list = classesFileRoleService.selectClassesFileRoleList(classesFileRole);
        ExcelUtil<ClassesFileRole> util = new ExcelUtil<ClassesFileRole>(ClassesFileRole.class);
        return util.exportExcel(list, "班级文档权限数据");
    }

    /**
     * 获取班级文档权限详细信息
     */
    @PreAuthorize("@ss.hasPermi('file:classesrole:query')")
    @GetMapping(value = "/{roleId}")
    public AjaxResult getInfo(@PathVariable("roleId") Long roleId)
    {
        return AjaxResult.success(classesFileRoleService.selectClassesFileRoleByRoleId(roleId));
    }

    /**
     * 新增班级文档权限
     */
    @PreAuthorize("@ss.hasPermi('file:classesrole:add')")
    @Log(title = "班级文档权限", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClassesFileRole classesFileRole)
    {
        return toAjax(classesFileRoleService.insertClassesFileRole(classesFileRole));
    }

    /**
     * 修改班级文档权限
     */
    @PreAuthorize("@ss.hasPermi('file:classesrole:edit')")
    @Log(title = "班级文档权限", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClassesFileRole classesFileRole)
    {
        return toAjax(classesFileRoleService.updateClassesFileRole(classesFileRole));
    }

    /**
     * 删除班级文档权限
     */
    @PreAuthorize("@ss.hasPermi('file:classesrole:remove')")
    @Log(title = "班级文档权限", businessType = BusinessType.DELETE)
	@DeleteMapping("/{roleIds}")
    public AjaxResult remove(@PathVariable Long[] roleIds)
    {
        return toAjax(classesFileRoleService.deleteClassesFileRoleByRoleIds(roleIds));
    }
}
