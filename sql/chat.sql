create table chat_file_data
(
    msg_id       mediumtext null,
    url          mediumtext null,
    file_name    mediumtext null,
    CREATED_TIME datetime   null,
    UPDATED_TIME datetime   null,
    deleted      mediumtext null
)
    comment '文件与请求地址';


create table chat_record
(
    CREATED_TIME datetime   null,
    UPDATED_TIME datetime   null,
    sender_id    mediumtext null,
    receiver_id  mediumtext null,
    message      mediumtext null,
    msg_type     int        null,
    if_read      mediumtext null,
    rb           mediumtext null,
    deleted      mediumtext null,
    msg_id       mediumtext null
)
    comment '聊天记录 ';

