CREATE TABLE visit_log_info(
                             visit_id BIGINT NOT NULL AUTO_INCREMENT  COMMENT '日志主键' ,
                             user_id BIGINT    COMMENT '用户ID' ,
                             visit_name varchar(30)   COMMENT '用户名',
                             method varchar(128)   COMMENT '方法名' ,
                             visit_url VARCHAR(128)    COMMENT 'URL' ,
                             update_time DATETIME    COMMENT '更新时间' ,
                             invoke_count INT    COMMENT '调用次数' ,
                             token_key varchar(128) COMMENT '用户的token值' ,
                             PRIMARY KEY (visit_id)
) COMMENT = ' ';

CREATE TABLE  visit_time_info(
                               time_id   BIGINT NOT NULL AUTO_INCREMENT  COMMENT '日志主键' ,
                               user_id   BIGINT    COMMENT '用户ID' ,
                               start_time DATETIME    COMMENT '开始时间' ,
                               leave_time DATETIME    COMMENT '离开时间' ,
                               time_long  bigint    COMMENT '访问时长' ,
                               token_key varchar(128) COMMENT '用户的token值' ,
                               PRIMARY KEY (time_id)
) COMMENT = ' ';
