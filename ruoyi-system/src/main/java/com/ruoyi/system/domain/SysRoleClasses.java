package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 角色和班级关联 sys_role_classes
 * 
 * @author ruoyi
 */
public class SysRoleClasses
{
    /** 角色ID */
    private Long roleId;
    
    /** 班级ID */
    private Long classesId;

    public Long getRoleId()
    {
        return roleId;
    }

    public void setRoleId(Long roleId)
    {
        this.roleId = roleId;
    }

    public Long getClassesId()
    {
        return classesId;
    }

    public void setClassesId(Long classesId)
    {
        this.classesId = classesId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("roleId", getRoleId())
            .append("classesId", getClassesId())
            .toString();
    }
}
