package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysRoleClasses;

/**
 * 角色与班级关联表 数据层
 * 
 * @author ruoyi
 */
public interface SysRoleClassesMapper
{
    /**
     * 通过角色ID删除角色和班级关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleClassesByRoleId(Long roleId);

    /**
     * 批量删除角色班级关联信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleClasses(Long[] ids);

    /**
     * 查询班级使用数量
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    public int selectCountRoleClassesByClassesId(Long classesId);

    /**
     * 批量新增角色班级信息
     * 
     * @param roleClassesList 角色班级列表
     * @return 结果
     */
    public int batchRoleClasses(List<SysRoleClasses> roleClassesList);
}
