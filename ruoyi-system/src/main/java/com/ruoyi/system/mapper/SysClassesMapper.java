package com.ruoyi.system.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ruoyi.common.core.domain.entity.SysClasses;

/**
 * 班级管理 数据层
 * 
 * @author ruoyi
 */
public interface SysClassesMapper
{
    /**
     * 查询班级管理数据
     * 
     * @param classes 班级信息
     * @return 班级信息集合
     */
    public List<SysClasses> selectClassesList(SysClasses classes);

    /**
     * 根据角色ID查询班级树信息
     * 
     * @param roleId 角色ID
     * @param classesCheckStrictly 班级树选择项是否关联显示
     * @return 选中班级列表
     */
    public List<Integer> selectClassesListByRoleId(@Param("roleId") Long roleId, @Param("classesCheckStrictly") boolean classesCheckStrictly);

    /**
     * 根据班级ID查询信息
     * 
     * @param classesId 班级ID
     * @return 班级信息
     */
    public SysClasses selectClassesById(Long classesId);

    /**
     * 根据ID查询所有子班级
     * 
     * @param classesId 班级ID
     * @return 班级列表
     */
    public List<SysClasses> selectChildrenClassesById(Long classesId);

    /**
     * 根据ID查询所有子班级（正常状态）
     * 
     * @param classesId 班级ID
     * @return 子班级数
     */
    public int selectNormalChildrenClassesById(Long classesId);

    /**
     * 是否存在子节点
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    public int hasChildByClassesId(Long classesId);

    /**
     * 查询班级是否存在用户
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    public int checkClassesExistUser(Long classesId);

    /**
     * 校验班级名称是否唯一
     * 
     * @param classesName 班级名称
     * @param parentId 父班级ID
     * @return 结果
     */
    public SysClasses checkClassesNameUnique(@Param("classesName") String classesName, @Param("parentId") Long parentId);

    /**
     * 新增班级信息
     * 
     * @param classes 班级信息
     * @return 结果
     */
    public int insertClasses(SysClasses classes);

    /**
     * 修改班级信息
     * 
     * @param classes 班级信息
     * @return 结果
     */
    public int updateClasses(SysClasses classes);

    /**
     * 修改所在班级正常状态
     * 
     * @param classesIds 班级ID组
     */
    public void updateClassesStatusNormal(Long[] classesIds);

    /**
     * 修改子元素关系
     * 
     * @param classess 子元素
     * @return 结果
     */
    public int updateClassesChildren(@Param("classess") List<SysClasses> classess);

    /**
     * 删除班级管理信息
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    public int deleteClassesById(Long classesId);
}
