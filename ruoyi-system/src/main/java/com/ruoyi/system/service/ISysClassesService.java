package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.core.domain.entity.SysClasses;

/**
 * 班级管理 服务层
 * 
 * @author ruoyi
 */
public interface ISysClassesService
{
    /**
     * 查询班级管理数据
     * 
     * @param classes 班级信息
     * @return 班级信息集合
     */
    public List<SysClasses> selectClassesList(SysClasses classes);

    /**
     * 构建前端所需要树结构
     * 
     * @param classess 班级列表
     * @return 树结构列表
     */
    public List<SysClasses> buildClassesTree(List<SysClasses> classess);

    /**
     * 构建前端所需要下拉树结构
     * 
     * @param classess 班级列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildClassesTreeSelect(List<SysClasses> classess);

    /**
     * 根据角色ID查询班级树信息
     * 
     * @param roleId 角色ID
     * @return 选中班级列表
     */
    public List<Integer> selectClassesListByRoleId(Long roleId);

    /**
     * 根据班级ID查询信息
     * 
     * @param classesId 班级ID
     * @return 班级信息
     */
    public SysClasses selectClassesById(Long classesId);

    /**
     * 根据ID查询所有子班级（正常状态）
     * 
     * @param classesId 班级ID
     * @return 子班级数
     */
    public int selectNormalChildrenClassesById(Long classesId);

    /**
     * 是否存在班级子节点
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    public boolean hasChildByClassesId(Long classesId);

    /**
     * 查询班级是否存在用户
     * 
     * @param classesId 班级ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkClassesExistUser(Long classesId);

    /**
     * 校验班级名称是否唯一
     * 
     * @param classes 班级信息
     * @return 结果
     */
    public String checkClassesNameUnique(SysClasses classes);

    /**
     * 校验班级是否有数据权限
     * 
     * @param classesId 班级id
     */
    public void checkClassesDataScope(Long classesId);

    /**
     * 新增保存班级信息
     * 
     * @param classes 班级信息
     * @return 结果
     */
    public int insertClasses(SysClasses classes);

    /**
     * 修改保存班级信息
     * 
     * @param classes 班级信息
     * @return 结果
     */
    public int updateClasses(SysClasses classes);

    /**
     * 删除班级管理信息
     * 
     * @param classesId 班级ID
     * @return 结果
     */
    public int deleteClassesById(Long classesId);
}
