## 开发


```bash

# 进入项目目录
cd bqinfo-ui
#若没有Node.js,请自行下载
# 安装依赖
npm install

# 建议不要直接使用 npm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```



浏览器访问 http://localhost:80



### 聊天模块相关

1. 部署minio [MinIO | The MinIO Quickstart Guide](https://docs.min.io/)

2. chat模块建表语句在sql/chat.sql

3. 配置文件 application.yml 中修改

   ```yaml
   chat:
     minio:
       url: http://172.30.182.9:9000/
       user: minioadmin
       pwd: minioadmin
   ```

4. 测试用app源码在mobile-app/test/ChatDemo 目前版本测试用例仅仅实现了最基本的相互发文件和发信息功能

5. api文档 https://docs.apipost.cn/preview/bd2c47442c6fcaed/005caf3602053136

### 班级模块相关

1. 按学院年级查询班级列表
2. 按Id查询班级详细信息
3. api文档：https://docs.apipost.cn/preview/3e74270dbacf39db/668de1050a9aee14


### app用户模块相关

1. app用户注册
2. app用户登录
3. 修改密码
4. 修改用户信息
5. 获取班级联系人
6. 根据学号获取用户信息
7. 根据ID获取用户信息
8. api文档：https://docs.apipost.cn/preview/3e74270dbacf39db/668de1050a9aee14


### 文件上传模块相关

1. 文件上传
2. 文件下载
3. 设置文件查看权限(待完成)
4. 按上传用户和班级获取文件列表
5. 根据ID查询文件详细信息
6. 根据ID删除文件
7. api文档：https://docs.apipost.cn/preview/3e74270dbacf39db/668de1050a9aee14


## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```