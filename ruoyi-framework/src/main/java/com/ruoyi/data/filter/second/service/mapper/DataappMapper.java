package com.ruoyi.data.filter.second.service.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DataappMapper {

    /**
     * 通过appId获取appKey
     * @param appId
     * @return
     */
    public String selectAppKeyByAppId(Long appId);
}
