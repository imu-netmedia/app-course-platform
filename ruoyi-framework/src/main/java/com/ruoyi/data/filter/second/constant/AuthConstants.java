package com.ruoyi.data.filter.second.constant;

/**
 * Created by wanggenshen
 * Date: on 2019/12/10 20:04.
 * Description: 常量
 */
public class AuthConstants {

    public static final String APP_KEY = "appKey";
    public static final String TIME_STAMP = "timeStamp";
    public static final String APP_ID = "appId";
    public static final String TOKEN = "token";


}
