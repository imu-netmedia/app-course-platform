package com.ruoyi.data.filter.second;


import com.ruoyi.data.filter.GetBeanUtil;
import com.ruoyi.data.filter.second.entity.ApiRequest;
import com.ruoyi.data.filter.second.service.ApiAuthencator;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccessFilter implements Filter {


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        StringBuffer url = request.getRequestURL();
        String appId = request.getParameter("appId");
        String token = request.getParameter("token");
        String currentTime = request.getParameter("timeStamp");
        ApiAuthencator apiAuthencator = GetBeanUtil.getBean(ApiAuthencator.class);
        int code = apiAuthencator.auth(new ApiRequest(token,Long.valueOf(appId),Long.valueOf(currentTime)),request,response);
        if(code != 1){
            return;
        }
        filterChain.doFilter(request,response);

    }
}
