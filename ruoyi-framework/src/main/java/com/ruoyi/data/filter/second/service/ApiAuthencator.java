package com.ruoyi.data.filter.second.service;


import com.ruoyi.data.filter.second.entity.ApiRequest;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public interface ApiAuthencator {

    int auth(ApiRequest apiRequest , ServletRequest request, ServletResponse response);
}
