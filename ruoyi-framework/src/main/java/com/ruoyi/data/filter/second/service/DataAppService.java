package com.ruoyi.data.filter.second.service;


public interface DataAppService {

    /**
     * 通过appId获取appKey
     * @param appId
     * @return
     */
    public String selectAppKeyByAppId(Long appId);
}
