package com.ruoyi.data.filter.first;

import com.ruoyi.common.core.domain.entity.data.AccessBody;
import com.ruoyi.common.core.domain.entity.data.JwtAccessToken;
import io.jsonwebtoken.JwtException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class AccessTokenProvider implements AuthenticationProvider {


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            JwtAccessToken jwtAccessToken = (JwtAccessToken) authentication;
            return jwtAccessToken;
        }catch (JwtException e){
            throw new AuthenticationServiceException("Token invalidate");
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(JwtAccessToken.class);
    }
}
