package com.ruoyi.data.filter.first;

import com.ruoyi.common.core.domain.entity.data.JwtAccessToken;
import com.ruoyi.common.core.domain.entity.data.AccessBody;
import com.ruoyi.data.filter.GetBeanUtil;
import com.ruoyi.data.filter.first.AccessTokenFailureHandler;
import com.ruoyi.data.filter.first.AccessTokenService;
import com.ruoyi.data.filter.first.AccessTokenSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestHeaderRequestMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAccessTokenFilter extends AbstractAuthenticationProcessingFilter {

    @Autowired
    public AccessTokenSuccessHandler accessTokenSuccessHandler;

    @Autowired
    private AccessTokenFailureHandler accessTokenFailureHandler;

    @Autowired
    public AccessTokenService accessTokenService;


    //拦截请求头为AccessToken的地址
    public JwtAccessTokenFilter(){
        super(new RequestHeaderRequestMatcher("AccessToken"));
//        super("/data/app/list");
    }


//    @Override
//    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) req;
//        HttpServletResponse response = (HttpServletResponse) res;
//
//        JwtAccessToken jwtAccessToken;
//        try {
//            jwtAccessToken =(JwtAccessToken)attemptAuthentication(request, response);
//            if (jwtAccessToken.getPrincipal() == null) {
//                return;
//            }
//        }
//        catch (InternalAuthenticationServiceException failed) {
//            unsuccessfulAuthentication(request, response, failed);
//            return;
//        }
//        catch (AuthenticationException failed) {
//            // Authentication failed
//            unsuccessfulAuthentication(request, response, failed);
//            return;
//        }
//
//        successfulAuthentication(request,response,chain,jwtAccessToken);
//    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {

        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Type", "text/html;charset=UTF-8");
        AccessTokenService accessTokenService = GetBeanUtil.getBean(AccessTokenService.class);
        AccessBody  accessBody = accessTokenService.getAccessBody(request);
        if(accessBody != null){
            accessTokenService.verifyToken(accessBody);
        }
        return getAuthenticationManager().authenticate(new JwtAccessToken(null,accessBody, accessBody == null ? null:accessBody.getAccessToken()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        AccessTokenSuccessHandler accessTokenSuccessHandler = GetBeanUtil.getBean(AccessTokenSuccessHandler.class);
        accessTokenSuccessHandler.onAuthenticationSuccess(request,response,chain,authResult);
//        chain.doFilter(request,response);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        AccessTokenFailureHandler accessTokenFailureHandler = GetBeanUtil.getBean(AccessTokenFailureHandler.class);
        accessTokenFailureHandler.onAuthenticationFailure(request,response,failed);
    }



}
