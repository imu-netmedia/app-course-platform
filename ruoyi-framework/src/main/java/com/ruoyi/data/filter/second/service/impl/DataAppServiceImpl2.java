package com.ruoyi.data.filter.second.service.impl;

import com.ruoyi.data.filter.second.service.DataAppService;
import com.ruoyi.data.filter.second.service.mapper.DataappMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DataAppServiceImpl2 implements DataAppService {

    @Autowired
    private DataappMapper dataappMapper;

    @Override
    public String selectAppKeyByAppId(Long appId) {
        return dataappMapper.selectAppKeyByAppId(appId);
    }
}
