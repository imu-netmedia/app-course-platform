package com.ruoyi.common.core.domain.model;

import com.ruoyi.common.core.domain.entity.SysUser;

/**
 * 用户注册对象
 * 
 * @author ruoyi
 */
public class RegisterBody extends SysUser
{

    /**
     * 验证码
     */
    private String code;

    /**
     * 唯一标识
     */
    private String uuid = "";

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
