package com.ruoyi.common.core.domain.entity.data;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtAccessToken extends AbstractAuthenticationToken {

    private final AccessBody accessBody;

    private final Object credentials;

    public JwtAccessToken(Collection<? extends GrantedAuthority> authorities, AccessBody accessBody, Object credentials) {
        super(authorities);
        this.accessBody = accessBody;
        this.credentials = credentials;
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    @Override
    public AccessBody getPrincipal() {
        return accessBody;
    }
}
