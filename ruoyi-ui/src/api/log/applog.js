import request from '@/utils/request'

// 查询 日志列表
export function listApplog(query) {
  return request({
    url: '/log/applog/list',
    method: 'get',
    params: query
  })
}

// 查询 日志详细
export function getApplog(timeId) {
  return request({
    url: '/log/applog/' + timeId,
    method: 'get'
  })
}

// 新增 日志
export function addApplog(data) {
  return request({
    url: '/log/applog',
    method: 'post',
    data: data
  })
}

// 修改 日志
export function updateApplog(data) {
  return request({
    url: '/log/applog',
    method: 'put',
    data: data
  })
}

// 详细 日志
export function detailsApplog(query) {
  return request({
    url: '/log/applog/details',
    method: 'get',
    params:query
  })
}

// 导出 日志
export function exportApplog(query) {
  return request({
    url: '/log/applog/export',
    method: 'get',
    params: query
  })
}
