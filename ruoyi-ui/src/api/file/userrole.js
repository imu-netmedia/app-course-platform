import request from '@/utils/request'

// 查询用户文档权限列表
export function listUserrole(query) {
  return request({
    url: '/file/userrole/list',
    method: 'get',
    params: query
  })
}

// 查询用户文档权限详细
export function getUserrole(roleId) {
  return request({
    url: '/file/userrole/' + roleId,
    method: 'get'
  })
}

// 新增用户文档权限
export function addUserrole(data) {
  return request({
    url: '/file/userrole',
    method: 'post',
    data: data
  })
}

// 修改用户文档权限
export function updateUserrole(data) {
  return request({
    url: '/file/userrole',
    method: 'put',
    data: data
  })
}

// 删除用户文档权限
export function delUserrole(roleId) {
  return request({
    url: '/file/userrole/' + roleId,
    method: 'delete'
  })
}

// 导出用户文档权限
export function exportUserrole(query) {
  return request({
    url: '/file/userrole/export',
    method: 'get',
    params: query
  })
}