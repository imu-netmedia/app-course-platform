import request from '@/utils/request'

// 查询班级文档权限列表
export function listClassesrole(query) {
  return request({
    url: '/file/classesrole/list',
    method: 'get',
    params: query
  })
}

// 查询班级文档权限详细
export function getClassesrole(roleId) {
  return request({
    url: '/file/classesrole/' + roleId,
    method: 'get'
  })
}

// 新增班级文档权限
export function addClassesrole(data) {
  return request({
    url: '/file/classesrole',
    method: 'post',
    data: data
  })
}

// 修改班级文档权限
export function updateClassesrole(data) {
  return request({
    url: '/file/classesrole',
    method: 'put',
    data: data
  })
}

// 删除班级文档权限
export function delClassesrole(roleId) {
  return request({
    url: '/file/classesrole/' + roleId,
    method: 'delete'
  })
}

// 导出班级文档权限
export function exportClassesrole(query) {
  return request({
    url: '/file/classesrole/export',
    method: 'get',
    params: query
  })
}