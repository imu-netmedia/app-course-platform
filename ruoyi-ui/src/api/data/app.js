import request from '@/utils/request'
import { praseStrEmpty } from "@/utils/ruoyi";


// 查询用户列表
export function listApp(query) {
  return request({
    url: '/data/app/list',
    method: 'get',
    params: query
  })
}

// 申请key值
export function applyKey(appId) {
  return request({
    url: '/data/app/getKey' +"?appId="+appId ,
    method: 'get',
  })
}


// 查询应用详细
export function getApp(id) {
  return request({
    url: '/data/app/' + praseStrEmpty(id),
    method: 'get'
  })
}

// 新增应用
export function addApp(data) {
  return request({
    url: '/data/app',
    method: 'post',
    data: data
  })
}

// 修改应用
export function editApp(data) {
  return request({
    url: '/data/app',
    method: 'put',
    data: data
  })
}

// 删除应用
export function delApp(id) {
  return request({
    url: '/data/app/' + id,
    method: 'delete'
  })
}

// 删除应用
export function testTest() {
  return request({
    url: '/testUrl/testPrint',
    method: 'get'
  })
}


