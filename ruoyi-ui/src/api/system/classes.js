import request from '@/utils/request'

// 查询班级列表
export function listClasses(query) {
  return request({
    url: '/system/classes/list',
    method: 'get',
    params: query
  })
}

// 查询班级列表（排除节点）
export function listClassesExcludeChild(classesId) {
  return request({
    url: '/system/classes/list/exclude/' + classesId,
    method: 'get'
  })
}

// 查询班级详细
export function getClasses(classesId) {
  return request({
    url: '/system/classes/' + classesId,
    method: 'get'
  })
}

// 查询班级下拉树结构
export function treeselect() {
  return request({
    url: '/system/classes/treeselect',
    method: 'get'
  })
}

// 根据角色ID查询班级树结构
export function roleClassesTreeselect(roleId) {
  return request({
    url: '/system/classes/roleClassesTreeselect/' + roleId,
    method: 'get'
  })
}

// 新增班级
export function addClasses(data) {
  return request({
    url: '/system/classes',
    method: 'post',
    data: data
  })
}

// 修改班级
export function updateClasses(data) {
  return request({
    url: '/system/classes',
    method: 'put',
    data: data
  })
}

// 删除班级
export function delClasses(classesId) {
  return request({
    url: '/system/classes/' + classesId,
    method: 'delete'
  })
}
